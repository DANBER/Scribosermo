import json

import numpy as np
import soundfile as sf
import tensorflow as tf

# ==================================================================================================

checkpoint_dir = "/checkpoints/en/conformerctc-large/exported/"
checkpoint_files = checkpoint_dir + "pb/"

alphabet_path = checkpoint_dir + "alphabet.json"
test_wav_path = "/Scribosermo/exporting/data/test_en.wav"
labels_path = "/Scribosermo/exporting/data/labels.json"

with open(alphabet_path, "r", encoding="utf-8") as file:
    alphabet = json.load(file)
    alphabet = [a.replace("▁", " ") for a in alphabet]

with open(labels_path, "r", encoding="utf-8") as file:
    labels = json.load(file)

idx2char = dict(enumerate(alphabet))

# ==================================================================================================


def load_audio(path):
    """Load wav file with the required format"""

    audio, _ = sf.read(path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def print_prediction_greedy(model, wav_path):
    """Simple greedy decoding of the network's prediction"""

    audio = load_audio(wav_path)
    prediction = model.predict(audio, steps=1)

    tpred = tf.transpose(prediction, perm=[1, 0, 2])
    logit_lengths = tf.constant(tf.shape(tpred)[0], shape=(1,))
    decoded = tf.nn.ctc_greedy_decoder(tpred, logit_lengths, merge_repeated=True)

    values = tf.cast(decoded[0][0].values, dtype=tf.int32).numpy()
    gd_text = "".join([idx2char[v] for v in values])

    # Text might contain spaces at the beginning or end
    gd_text = gd_text.strip()
    print("Prediction greedy: {}".format(gd_text))


# ==================================================================================================


def main():

    # Load model and print some infos about it
    print("\nLoading model ...")
    model = tf.keras.models.load_model(checkpoint_files)
    model.summary()
    print("Metadata: {}\n".format(model.get_metadata()))

    print("\nTranscribing the audio ...")

    # Print label if one of the test wavs is used
    if test_wav_path.startswith("/Scribosermo/exporting/data/test_"):
        lang = test_wav_path[-6:-4]
        if lang in labels:
            print("Label:            ", labels[lang])

    # Now run the transcription
    print_prediction_greedy(model, test_wav_path)


# ==================================================================================================

if __name__ == "__main__":
    main()
