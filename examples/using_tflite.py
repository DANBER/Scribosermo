import json
import multiprocessing as mp

import numpy as np
import soundfile as sf
import tflite_runtime.interpreter as tflite

# ==================================================================================================

checkpoint_dir = "/checkpoints/en/conformerctc-large/exported/"
checkpoint_file = checkpoint_dir + "model_float32.tflite"

alphabet_path = checkpoint_dir + "alphabet.json"
test_wav_path = "/Scribosermo/exporting/data/test_en.wav"
labels_path = "/Scribosermo/exporting/data/labels.json"

with open(alphabet_path, "r", encoding="utf-8") as file:
    alphabet = json.load(file)
    alphabet = [a.replace("▁", " ") for a in alphabet]

with open(labels_path, "r", encoding="utf-8") as file:
    labels = json.load(file)

idx2char = dict(enumerate(alphabet))

# ==================================================================================================


def load_audio(path):
    """Load wav file with the required format"""

    audio, _ = sf.read(path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict(interpreter, audio):
    """Feed an audio signal with shape [1, len_signal] into the network and get the predictions"""

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    # Feed audio
    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    return output_data


# ==================================================================================================


def greedy_decode(ctc_labes: np.ndarray) -> str:
    """Simple greedy decoding of the network's prediction"""

    # Greedy ctc decoding
    values = np.argmax(ctc_labes, axis=-1)
    merged = [values[0]]
    for v in values[1:]:
        if v != merged[-1]:
            merged.append(v)
    merged = [v for v in merged if not v == len(alphabet)]
    gd_text = "".join([idx2char[v] for v in merged])

    # Text might contain spaces at the beginning or end
    gd_text = gd_text.strip()

    return gd_text


# ==================================================================================================


def print_prediction(interpreter, wav_path):

    audio = load_audio(wav_path)
    prediction = predict(interpreter, audio)
    text = greedy_decode(prediction[0])
    print("Prediction greedy: {}".format(text))


# ==================================================================================================


def main():

    # Load model and print some infos about it
    print("\nLoading model ...")
    interpreter = tflite.Interpreter(
        model_path=checkpoint_file, num_threads=mp.cpu_count()
    )
    print("Input details:", interpreter.get_input_details())

    print("\nTranscribing the audio ...")

    # Print label if one of the test wavs is used
    if test_wav_path.startswith("/Scribosermo/exporting/data/test_"):
        lang = test_wav_path[-6:-4]
        if lang in labels:
            print("Label:            ", labels[lang])

    # Now run the transcription
    print_prediction(interpreter, test_wav_path)


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("\nFINISHED")
