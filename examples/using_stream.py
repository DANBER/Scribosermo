# Most of the streaming concept is taken from Nvidia's reference implementation:
# https://github.com/NVIDIA/NeMo/blob/main/tutorials/asr/02_Online_ASR_Microphone_Demo.ipynb

import json
import multiprocessing as mp

import numpy as np
import soundfile as sf
import tflite_runtime.interpreter as tflite

# ==================================================================================================

checkpoint_dir = "/checkpoints/en/conformerctc-large/exported/"
checkpoint_file = checkpoint_dir + "model_float32.tflite"
alphabet_path = checkpoint_dir + "alphabet.json"
test_wav_path = "/Scribosermo/exporting/data/test_en.wav"
sample_rate = 16000

# Experiment a little with those values to optimize inference
chunk_size = int(1.0 * sample_rate)
frame_overlap = int(2.0 * sample_rate)
char_offset = 5

with open(alphabet_path, "r", encoding="utf-8") as file:
    alphabet = json.load(file)
    alphabet = [a.replace("▁", " ") for a in alphabet]

idx2char = dict(enumerate(alphabet))


# ==================================================================================================


def load_audio(path):
    """Load wav file with the required format"""

    audio, _ = sf.read(path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict(interpreter, audio):
    """Feed an audio signal with shape [1, len_signal] into the network and get the predictions"""

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    # Feed audio
    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    return output_data


# ==================================================================================================


def greedy_decode(ctc_labes: np.ndarray):
    """Simple greedy decoding of the network's prediction"""

    # Greedy ctc decoding
    values = np.argmax(ctc_labes, axis=-1)
    merged = [values[0]]
    for v in values[1:]:
        if v != merged[-1]:
            merged.append(v)
    merged = [v for v in merged if not v == len(alphabet)]
    gd_text = "".join([idx2char[v] for v in merged])

    # Text might contain spaces at the beginning or end
    gd_text = gd_text.strip()

    return gd_text


# ==================================================================================================


def feed_chunk(chunk: np.ndarray, overlap: int, offset: int, interpreter) -> str:
    """Feed an audio chunk with shape [1, len_chunk] into the decoding process"""

    # Get network prediction for chunk
    prediction = predict(interpreter, chunk)
    prediction = prediction[0]

    # Extract the interesting part in the middle of the prediction
    timesteps_overlap = int(len(prediction) / (chunk.shape[1] / overlap)) - 2
    prediction = prediction[timesteps_overlap:-timesteps_overlap]

    # Apply some offset for improved results
    prediction = prediction[: len(prediction) - offset]

    text = greedy_decode(prediction)
    return text


# ==================================================================================================


def streamed_transcription(interpreter, wav_path):
    """Transcribe an audio file chunk by chunk"""

    # For reasons of simplicity, a wav-file is used instead of a microphone stream
    audio = load_audio(wav_path)
    audio = audio[0]

    # Add some empty padding that the last words are not cut from the transcription
    audio = np.concatenate([audio, np.zeros(shape=frame_overlap, dtype=np.float32)])

    start = 0
    buffer = np.zeros(shape=2 * frame_overlap + chunk_size, dtype=np.float32)
    while start < len(audio):

        # Cut a chunk from the complete audio signal
        stop = min(len(audio), start + chunk_size)
        chunk = audio[start:stop]
        start = stop

        # Add new frames to the end of the buffer
        buffer = buffer[chunk_size:]
        buffer = np.concatenate([buffer, chunk])

        # Now feed this frame into the decoding process
        ibuffer = np.expand_dims(buffer, axis=0)
        text = feed_chunk(ibuffer, frame_overlap, char_offset, interpreter)

        print('"{}"'.format(text))


# ==================================================================================================


def main():

    print("\nLoading model ...")
    interpreter = tflite.Interpreter(
        model_path=checkpoint_file, num_threads=mp.cpu_count()
    )

    print("Running transcription ...\n")
    streamed_transcription(interpreter, test_wav_path)


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("\nFINISHED")
