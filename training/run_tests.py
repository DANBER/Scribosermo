import tensorflow as tf
import tqdm

from scode import nets, pipeline, testing, utils

# ==================================================================================================


def test_pipeline(sample_csv_path):
    bench_batch_size = 16
    config = utils.get_config()

    # Run pipeline for one epoch to check how long preprocessing takes
    print("\nGoing through dataset to check preprocessing duration...")
    tds = pipeline.create_pipeline(
        sample_csv_path, bench_batch_size, config, mode="train"
    )
    for _ in tqdm.tqdm(tds):
        pass

    # Print the first sample of the dataset
    tds = pipeline.create_pipeline(sample_csv_path, 1, config, mode="train")
    for samples in tds:
        print(samples)
        break


# ==================================================================================================


def print_config():
    config = utils.get_config()
    print(config)


# ==================================================================================================


def test_model():
    config = utils.get_config()

    network_type = config["network"]["name"]
    c_input = config["audio_features"]["num_features"]
    c_output = len(utils.load_alphabet(config)) + 1

    # Create the network
    mynet = getattr(nets, network_type)
    model = mynet.MyModel(
        c_input=c_input, c_output=c_output, netconfig=config["network"]
    )

    model.build(input_shape=(None, None, c_input))
    model.summary()

    flops = utils.get_flops(model, ntime=458, nfeat=c_input) / 1e9
    print("The model has {:.2f}G multiply-add operations".format(flops))

    inp = tf.random.normal([1, 458, c_input])
    out = model(inp)
    print("Input:", tf.shape(inp))
    print("Output:", tf.shape(out))


# ==================================================================================================

if __name__ == "__main__":
    print("\n======================================================================\n")

    testing.main()

    # print_config()
    # test_pipeline("/data_prepared/en/librispeech/test-clean_azce.csv")
    # test_model()
    print("FINISHED")
