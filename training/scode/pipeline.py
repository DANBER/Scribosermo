import librosa
import pandas as pd
import tensorflow as tf

from . import augmentations, utils

# ==================================================================================================

AUTOTUNE = tf.data.experimental.AUTOTUNE
char2idx: tf.lookup.StaticHashTable
audio_sample_rate: int
audio_window_samples: int
audio_step_samples: int
num_features: int

# ==================================================================================================


def initialize(config):
    global char2idx, audio_sample_rate, audio_window_samples, audio_step_samples, num_features

    alphabet = utils.load_alphabet(config)
    char2idx = tf.lookup.StaticHashTable(
        initializer=tf.lookup.KeyValueTensorInitializer(
            keys=tf.constant([u for i, u in enumerate(alphabet)]),
            values=tf.constant([i for i, u in enumerate(alphabet)]),
        ),
        default_value=tf.constant(0),
    )

    audio_sample_rate = int(config["audio_features"]["audio_sample_rate"])
    num_features = config["audio_features"]["num_features"]
    window_len = config["audio_features"]["window_len"]
    window_step = config["audio_features"]["window_step"]
    audio_window_samples = int(audio_sample_rate * window_len)
    audio_step_samples = int(audio_sample_rate * window_step)


# ==================================================================================================


def text_to_ids(sample):
    global char2idx

    text = tf.strings.lower(sample["text"])
    text_as_chars = tf.strings.unicode_split(text, "UTF-8")
    text_as_ints = char2idx.lookup(text_as_chars)
    sample["label"] = text_as_ints
    sample["label_length"] = tf.strings.length(text)
    return sample


# ==================================================================================================


def apply_augmentations(tensor, datatype: str, config: dict, train_mode: bool = False):
    """Checks which augmentations are selected and applies them"""

    if datatype == "signal":
        augs = config["augmentations"]["signal"]

        if "extra_silence" in augs:
            aug = augs["extra_silence"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.extra_silence(
                    tensor, int(audio_sample_rate * aug["time"])
                )

        if "dither" in augs:
            aug = augs["dither"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.dither(tensor, aug["factor"])

        if "normalize_volume" in augs:
            aug = augs["normalize_volume"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.normalize_volume(tensor)

        if "preemphasis" in augs:
            aug = augs["preemphasis"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.preemphasis(tensor, aug["coefficient"])

        if "resample" in augs:
            aug = augs["resample"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.resample(
                    tensor, audio_sample_rate, aug["tmp_sample_rate"]
                )

        if "random_volume" in augs:
            aug = augs["random_volume"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.random_volume(
                    tensor, aug["min_dbfs"], aug["min_dbfs"]
                )

        if "reverb" in augs:
            aug = augs["reverb"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.reverb(
                    tensor, audio_sample_rate, aug["delay"], aug["decay"]
                )

    if datatype == "spectrogram":
        augs = config["augmentations"]["spectrogram"]

        if "random_pitch" in augs:
            aug = augs["random_pitch"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.random_pitch(
                    tensor, aug["mean"], aug["stddev"], aug["cut_min"], aug["cut_max"]
                )

        if "random_speed" in augs:
            aug = augs["random_speed"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.random_speed(
                    tensor, aug["mean"], aug["stddev"], aug["cut_min"], aug["cut_max"]
                )

        if "freq_mask" in augs:
            aug = augs["freq_mask"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.freq_mask(tensor, aug["n"], aug["max_size"])

        if "time_mask" in augs:
            aug = augs["time_mask"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.time_mask(tensor, aug["n"], aug["max_size"])

        if "spec_cutout" in augs:
            aug = augs["spec_cutout"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.spec_cutout(
                    tensor, aug["n"], aug["max_freq_size"], aug["max_time_size"]
                )

        if "spec_dropout" in augs:
            aug = augs["spec_dropout"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.spec_dropout(tensor, aug["max_rate"])

    if datatype == "features":
        augs = config["augmentations"]["features"]

        if "random_multiply" in augs:
            aug = augs["random_multiply"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.random_multiply(
                    tensor, aug["mean"], aug["stddev"]
                )

        if "random_add" in augs:
            aug = augs["random_add"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.dither(tensor, aug["factor"])

        if "normalize_features" in augs:
            aug = augs["normalize_features"]
            if (aug["use_train"] and train_mode) or (
                aug["use_test"] and not train_mode
            ):
                tensor = augmentations.per_feature_norm(tensor)

    return tensor


# ==================================================================================================


def load_audio(sample):
    audio_binary = tf.io.read_file(sample["filepath"])
    audio, _ = tf.audio.decode_wav(audio_binary)

    sample["raw_audio"] = audio
    return sample


# ==================================================================================================


def augment_signal(sample, config: dict, train_mode: bool = False):
    audio = tf.squeeze(sample["raw_audio"], axis=-1)
    audio = apply_augmentations(audio, "signal", config, train_mode)

    audio = tf.expand_dims(audio, axis=-1)
    sample["signal"] = audio
    return sample


# ==================================================================================================


def audio_to_spect(sample, config: dict, train_mode: bool = False):
    global audio_window_samples, audio_step_samples

    # Pytorch uses a slightly different spectrogram calculation which is matched to librosa
    # unlike the default tensorflow implementation
    n_fft = 512
    nbatch = tf.shape(sample["signal"])[0]

    # Add center padding
    signal = tf.reshape(sample["signal"], [nbatch, -1])
    pad_amount = int(audio_window_samples // 2)
    signal = tf.pad(signal, [[0, 0], [pad_amount, pad_amount]], "REFLECT")
    signal = tf.reshape(signal, [nbatch, 1, -1])

    # Calculate short-time Fourier transforms with a differnt windowing approach
    f = tf.signal.frame(signal, audio_window_samples, audio_step_samples, pad_end=False)
    w = tf.signal.hann_window(audio_window_samples, periodic=False)
    stfts = tf.signal.rfft(f * w, fft_length=[n_fft])

    # Obtain the magnitude of the STFT.
    spectrogram = tf.math.real(stfts) ** 2 + tf.math.imag(stfts) ** 2
    spectrogram = tf.squeeze(spectrogram, axis=1)

    spectrogram = apply_augmentations(spectrogram, "spectrogram", config, train_mode)
    sample["spectrogram"] = spectrogram
    return sample


# ==================================================================================================


def audio_to_lfbank(sample, config: dict, train_mode: bool = False):
    """Calculate log mel filterbanks from spectrogram"""
    global audio_sample_rate, num_features

    # The pytorch models of NeMo use a different mel conversion approach than the
    # default tensorflow implementation
    n_fft = 512
    lin2mel_matrix = librosa.filters.mel(
        sr=audio_sample_rate,
        n_fft=n_fft,
        n_mels=num_features,
        fmin=0,
        fmax=audio_sample_rate / 2,
    )
    lin2mel_matrix = tf.transpose(tf.constant(lin2mel_matrix))
    zero_guard = 2**-24

    mel_spectrograms = tf.tensordot(sample["spectrogram"], lin2mel_matrix, 1)
    mel_spectrograms.set_shape(
        sample["spectrogram"].shape[:-1].concatenate(lin2mel_matrix.shape[-1:])
    )
    features = tf.math.log(mel_spectrograms + zero_guard)

    features = apply_augmentations(features, "features", config, train_mode)
    sample["features"] = features
    return sample


# ==================================================================================================


def create_pipeline(csv_path: str, batch_size: int, config: dict, mode: str):
    """Create data-iterator from tab separated csv file"""
    global num_features

    # Initialize pipeline values, using config from method call, that we can easily reuse the config
    # from exported checkpoints
    initialize(config)

    # Keep the german 0 as "null" string
    df = pd.read_csv(csv_path, encoding="utf-8", sep="\t", keep_default_na=False)
    df = df[["filepath", "duration", "text"]]

    if config["training"]["sort_datasets"]:
        df = df.sort_values(
            "duration", ascending=config["training"]["sort_ds_ascending"]
        )

    df = df[["filepath", "text"]]
    ds = tf.data.Dataset.from_tensor_slices(dict(df))

    # Generate text labels
    ds = ds.map(text_to_ids, num_parallel_calls=AUTOTUNE)

    # Load audio from files
    ds = ds.map(map_func=load_audio, num_parallel_calls=AUTOTUNE)

    # LSTM networks seem to have problems with half filled batches
    # Drop them in training and evaluation, but keep them while testing
    drop_remainder = bool(mode in ["train", "eval"])

    # Instead of batching and padding the samples at the end, pad them directly after loading the
    # audio file. In this way only "silence" is added to the end of the audio signal which the model
    # should ignore. Since the files are sorted this shouldn't be much.
    if batch_size == 1:
        # No need for padding here
        # This also makes debugging easier if the key dropping is skipped
        ds = ds.batch(1)
    else:
        ds = ds.padded_batch(
            batch_size=batch_size,
            drop_remainder=drop_remainder,
            padded_shapes=(
                {
                    "raw_audio": [None, 1],
                    "text": [],
                    "filepath": [],
                    "label": [None],
                    "label_length": [],
                }
            ),
        )

    # Apply augmentations only in training
    train_mode = bool(mode in ["train"])

    # Shuffle the batches. A larger buffer size is better but requires more memory.
    if train_mode and config["augmentations"]["shuffle_train"]:
        ds = ds.shuffle(buffer_size=1024, reshuffle_each_iteration=True)

    as_func = lambda x: augment_signal(x, config, train_mode)
    ds = ds.map(map_func=as_func, num_parallel_calls=AUTOTUNE)

    # Calculate spectrogram
    a2s_func = lambda x: audio_to_spect(x, config, train_mode)
    ds = ds.map(map_func=a2s_func, num_parallel_calls=AUTOTUNE)

    # Calculate audio features
    a2f_func = lambda x: audio_to_lfbank(x, config, train_mode)
    ds = ds.map(map_func=a2f_func, num_parallel_calls=AUTOTUNE)

    ds = ds.prefetch(buffer_size=16)
    return ds
