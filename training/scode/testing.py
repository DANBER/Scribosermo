import json
import os
from typing import List

import fastwer
import tensorflow as tf
import tqdm
from pyctcdecode import BeamSearchDecoderCTC, build_ctcdecoder

from . import pipeline, training, utils

# ==================================================================================================

run_on_gpu_id = 0
config = utils.get_config()
checkpoint_dir = config["checkpoint_dir"]
model: tf.keras.Model
idx2char: tf.lookup.StaticHashTable
decoder: BeamSearchDecoderCTC

# ==================================================================================================


def calc_stats(results: List[dict]) -> List[dict]:
    """Calculate CER and WER of both prediction types"""

    for result in results:
        label = result["label"]
        greedy_text = result["greedy_text"]
        lm_text = result["lm_text"]

        gd_wer = fastwer.score_sent(greedy_text, label)
        gd_cer = fastwer.score_sent(greedy_text, label, char_level=True)
        result["greedy_wer"] = gd_wer / 100.0
        result["greedy_cer"] = gd_cer / 100.0

        lm_wer = fastwer.score_sent(lm_text, label)
        lm_cer = fastwer.score_sent(lm_text, label, char_level=True)
        result["lm_wer"] = lm_wer / 100.0
        result["lm_cer"] = lm_cer / 100.0

    return results


# ==================================================================================================


def get_texts(predictions, samples):

    twfs = []
    for i, pred in enumerate(predictions):

        # Calculate text using language model. Use extra softmax to convert values from log_softmax
        # range to normal softmax range to prevent "Segmentation fault (core dumped)" error
        spred = tf.nn.softmax(pred)
        spred = spred.numpy()
        lm_text = decoder.decode(spred, beam_width=512)

        # Calculate greedy text
        gpred = tf.expand_dims(pred, axis=1)
        logit_lengths = tf.constant(tf.shape(gpred)[0], shape=(1,))
        gdecoded, _ = tf.nn.ctc_greedy_decoder(
            gpred, logit_lengths, merge_repeated=True
        )
        values = tf.cast(gdecoded[0].values, dtype=tf.int32)
        values = idx2char.lookup(values).numpy()
        greedy_text = b"".join(values).decode("utf-8")

        if config["language"] == "fr":
            # Replace characters that occur in the alphabet of the model but not in the labels
            lm_text = lm_text.replace("-", " ")
            greedy_text = greedy_text.replace("-", " ")
            lm_text = lm_text.replace("ì", "i")
            greedy_text = greedy_text.replace("ì", "i")

        # Remove spaces at the start, after another and the end
        lm_text = " ".join(lm_text.split())
        greedy_text = " ".join(greedy_text.split())

        # Get label
        label = samples["text"][i].numpy()
        label = label.decode("utf-8").strip()

        twf = {
            "filepath": samples["filepath"][i].numpy(),
            "label": label,
            "greedy_text": greedy_text,
            "lm_text": lm_text,
        }
        twfs.append(twf)

    return twfs


# ==================================================================================================


def print_results(results: List[dict]):
    """Prints test summary and worst predictions"""

    if config["logging"]["log_worst_test_predictions"] > 0:
        print(
            "\nPredictions with highest {}:".format(config["logging"]["sort_wtl_key"])
        )
        results = sorted(
            results, key=lambda r: r[config["logging"]["sort_wtl_key"]], reverse=True
        )
        wres = results[: config["logging"]["log_worst_test_predictions"]]

        keep_keys = ["filepath", "label", "greedy_text", "greedy_cer"]
        for wr in wres:
            print("-----")
            pr = {k: v for k, v in wr.items() if k in keep_keys}
            for k in pr:
                if k in ["label", "greedy_text"]:
                    print("-  {}: '{}'".format(k, pr[k]))
                else:
                    print("-  {}: {}".format(k, pr[k]))
        print("-----")

    gd_cer, gd_wer = 0, 0
    lm_cer, lm_wer = 0, 0
    for result in results:
        gd_cer += result["greedy_cer"]
        gd_wer += result["greedy_wer"]
        lm_cer += result["lm_cer"]
        lm_wer += result["lm_wer"]

    len_results = len(results)
    gd_cer = gd_cer / len_results
    gd_wer = gd_wer / len_results
    lm_cer = lm_cer / len_results
    lm_wer = lm_wer / len_results

    print("\nTest summary:")
    print("  CER greedy: {:.4f}".format(gd_cer))
    print("  CER with lm: {:.4f}".format(lm_cer))
    print("  WER greedy: {:.4f}".format(gd_wer))
    print("  WER with lm: {:.4f}".format(lm_wer))
    print("")


# ==================================================================================================


def run_test(dataset_test):
    print("\nEvaluating ...")
    step = 0
    log_greedy_steps = config["logging"]["log_prediction_steps"]

    test_results = []
    for samples in tqdm.tqdm(dataset_test):
        features = samples["features"]
        predictions = model.predict(features)
        step += 1

        results = get_texts(predictions, samples)
        test_results.extend(results)

        if log_greedy_steps != 0 and step % (log_greedy_steps * 4) == 0:
            print("=Label========: {}".format(results[0]["label"]))
            print("=GreedyText===: {}".format(results[0]["greedy_text"]))
            print("=LmText=======: {}".format(results[0]["lm_text"]))

    test_results = calc_stats(test_results)
    print_results(test_results)


# ==================================================================================================


def main():
    global model, decoder, idx2char

    print("Starting test with config:")
    print(json.dumps(config, indent=2))

    # Hide all but one gpus
    os.environ["CUDA_VISIBLE_DEVICES"] = str(run_on_gpu_id)

    # Allow growing gpu memory on first gpu
    gpus = tf.config.experimental.list_physical_devices("GPU")
    if len(gpus) > 0:
        tf.config.experimental.set_memory_growth(gpus[0], True)

    # Use exported config to set up the pipeline
    path = os.path.join(checkpoint_dir, "config_export.json")
    exported_config = utils.load_json_file(path)

    dataset_test = pipeline.create_pipeline(
        csv_path=config["data"]["test"],
        batch_size=config["batch_sizes"]["test"],
        config=config,
        mode="test",
    )

    # Load alphabet
    alphabet = utils.load_alphabet(exported_config)
    sp_alphabet = [a.replace(" ", "▁") for a in alphabet]

    # Load vocabulary
    vocab_path = config["langmodel"]["vocab_path"]
    with open(vocab_path, "r", encoding="utf-8") as file:
        vocab = file.readlines()
        vocab = [v.strip() for v in vocab]

    # Prepare decoder
    print("\nLoading decoder ...")
    decoder = build_ctcdecoder(
        sp_alphabet,
        kenlm_model_path=config["langmodel"]["arpa_path"],
        unigrams=vocab,
        alpha=config["langmodel"]["alpha"],
        beta=config["langmodel"]["beta"],
    )

    model = training.load_exported_model(checkpoint_dir)
    model.summary()

    training.model = model
    idx2char = training.create_idx2char()
    run_test(dataset_test)
