import librosa
import tensorflow as tf
from tensorflow.keras import layers as tfl

# ==================================================================================================


class AudioLayer(tfl.Layer):
    def __init__(
        self,
        audio_sample_rate: int,
        num_features: int,
        audio_window_samples: int,
        audio_step_samples: int,
        n_fft=512,
        int8_zero_guards=False,
    ):
        super().__init__()

        self.audio_window_samples = audio_window_samples
        self.audio_step_samples = audio_step_samples
        self.n_fft = n_fft

        # When using int8-quantization, the default zero-guard values are to small and converted
        # to zero themselve, therefore use larger values when quantization to int8-precision
        self.int8_zero_guards = int8_zero_guards

        # Create the matrix here, because building it in the make_model function raised errors
        # when loading the exported model
        self.lin2mel_matrix = librosa.filters.mel(
            sr=audio_sample_rate,
            n_fft=self.n_fft,
            n_mels=num_features,
            fmin=0,
            fmax=audio_sample_rate / 2,
        )

    # ==============================================================================================

    @staticmethod
    def preemphasis(signal, coef=0.97):
        """Emphasizes high-frequency signal components"""

        psig = signal[:, 1:] - coef * signal[:, :-1]
        signal = tf.concat([tf.expand_dims(signal[:, 0], axis=-1), psig], axis=1)
        return signal

    # ==============================================================================================

    def per_feature_norm(self, features):
        """Normalize features per channel/frequency"""
        f_mean = tf.math.reduce_mean(features, axis=1)
        f_mean = tf.expand_dims(f_mean, axis=1)

        # Calculate our own std, because pytorch uses unbiasing,
        # which is approximated by reducing the sample number by one
        f_std = features - f_mean
        f_std = tf.reduce_sum(f_std**2, axis=1)
        n = tf.cast(tf.shape(features)[1], dtype=f_std.dtype) - 1.0
        f_std = tf.sqrt(f_std / n)
        f_std = tf.expand_dims(f_std, axis=1)

        if not self.int8_zero_guards:
            zero_guard = 1e-5
        else:
            zero_guard = 1.0 / 255
        features = (features - f_mean) / (f_std + zero_guard)
        return features

    # ==============================================================================================

    def audio_to_spect(self, audio):
        """Calculate the spectrogram"""

        # Pytorch uses a slightly different spectrogram calculation which is matched to librosa
        # unlike the default tensorflow implementation
        nbatch = tf.shape(audio)[0]

        # Add center padding
        signal = tf.reshape(audio, [nbatch, -1])
        pad_amount = int(self.audio_window_samples // 2)
        signal = tf.pad(signal, [[0, 0], [pad_amount, pad_amount]], "REFLECT")
        signal = tf.reshape(signal, [nbatch, 1, -1])

        # Calculate short-time Fourier transforms with a differnt windowing approach
        f = tf.signal.frame(
            signal, self.audio_window_samples, self.audio_step_samples, pad_end=False
        )
        w = tf.signal.hann_window(self.audio_window_samples, periodic=False)
        stfts = tf.signal.rfft(f * w, fft_length=[self.n_fft])

        # Obtain the magnitude of the STFT.
        spectrogram = tf.math.real(stfts) ** 2 + tf.math.imag(stfts) ** 2
        spectrogram = tf.squeeze(spectrogram, axis=1)

        return spectrogram

    # ==============================================================================================

    def audio_to_lfbank(self, spectrogram):
        """Calculate log mel filterbanks from spectrogram"""

        lin2mel_matrix = tf.transpose(tf.constant(self.lin2mel_matrix))
        if not self.int8_zero_guards:
            zero_guard = 2**-24
        else:
            zero_guard = 2**-5

        mel_spectrograms = tf.tensordot(spectrogram, lin2mel_matrix, 1)
        mel_spectrograms.set_shape(
            spectrogram.shape[:-1].concatenate(lin2mel_matrix.shape[-1:])
        )
        features = tf.math.log(mel_spectrograms + zero_guard)

        return features

    # ==============================================================================================

    def call(self, x, training=False):  # pylint: disable=arguments-differ

        # Signal augmentations
        audio = self.preemphasis(x, coef=0.97)
        audio = tf.expand_dims(audio, axis=-1)

        # Spectrogram
        spectrogram = self.audio_to_spect(audio)

        # LogFilterbanks
        features = self.audio_to_lfbank(spectrogram)

        # Feature augmentation
        features = self.per_feature_norm(features)

        return features


# ==================================================================================================


class MyModel(tf.keras.Model):  # pylint: disable=abstract-method
    def __init__(self, nn_model, metadata, int8_zero_guards=False):
        super().__init__()

        self.metadata: dict = metadata
        self.nn_model = nn_model

        self.audio_layer = AudioLayer(
            int(self.metadata["audio_sample_rate"]),
            int(self.metadata["num_features"]),
            int(self.metadata["audio_window_samples"]),
            int(self.metadata["audio_step_samples"]),
            int8_zero_guards=int8_zero_guards,
        )

        self.model = self.make_model()

    # ==============================================================================================

    def make_model(self):
        input_tensor = tfl.Input(shape=[None], name="input_samples")

        # Used for easier debugging changes
        audio = tf.identity(input_tensor)

        # Convert audio signal to features
        features = self.audio_layer(audio)

        # Add a name to be able to split the prediction into sub-graphs
        x = tf.identity(features, name="features")

        # Get predictions
        x = self.nn_model(x, training=False)

        # Convert log-softmax to softmax for ctc decoding
        x = tf.nn.softmax(x)

        output_tensor = tf.identity(x, name="logits")

        name = "Exported{}".format(self.metadata["network"].title())
        model = tf.keras.Model(input_tensor, output_tensor, name=name)
        return model

    # ==============================================================================================

    # Input signature is required to export this method into ".pb" format and use it in inference
    @tf.function(input_signature=[])
    def get_time_reduction_factor(self):
        """Some models reduce the time dimension of the features, for example with striding."""
        return self.nn_model.get_time_reduction_factor()

    # ==============================================================================================

    # Input signature is required to export this method into ".pb" format and use it in inference
    @tf.function(input_signature=[])
    def get_metadata(self):
        """Return metadata for model"""
        return self.metadata

    # ==============================================================================================

    def summary(self):  # pylint: disable=arguments-differ
        print("")
        self.model.summary(line_length=100)

    # ==============================================================================================

    # This input signature is required that we can export and load the model in ".pb" format
    # with a variable sequence length, instead of using the one of the first input.
    @tf.function(input_signature=[tf.TensorSpec([None, None], tf.float32)])
    def call(self, x):  # pylint: disable=arguments-differ
        """Call with input shape: [1, len_signal], output shape: [1, len_steps, n_alphabet]"""

        x = self.model(x)
        return x
