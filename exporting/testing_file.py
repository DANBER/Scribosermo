import json
import multiprocessing as mp

import numpy as np
import soundfile as sf

# When testing on different devices not all runtimes might be installed
tflite_enabled = False
tf_enabled = False
try:
    import tflite_runtime.interpreter as tflite

    tflite_enabled = True
except ImportError:
    pass
try:
    import tensorflow as tf

    tf_enabled = True
except ImportError:
    pass


# ==================================================================================================

export_dir = "/checkpoints/en/conformerctc-large/exported/"
checkpoint_pb = export_dir + "pb/"
checkpoint_tflite_float32 = export_dir + "model_float32.tflite"
checkpoint_tflite_float16 = export_dir + "model_float16.tflite"
checkpoint_tflite_int8 = export_dir + "model_int8.tflite"

alphabet_path = export_dir + "alphabet.json"
wav_path = "/Scribosermo/exporting/data/test_en.wav"
labels_path = "/Scribosermo/exporting/data/labels.json"

with open(alphabet_path, "r", encoding="utf-8") as file:
    alphabet = json.load(file)
    alphabet = [a.replace("▁", " ") for a in alphabet]

with open(labels_path, "r", encoding="utf-8") as file:
    labels = json.load(file)

idx2char = dict(enumerate(alphabet))

# ==================================================================================================


def load_audio(path):
    """Load wav file with the required format"""

    audio, _ = sf.read(path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict(interpreter, audio):
    """Feed an audio signal with shape [1, len_signal] into the network and get the predictions"""

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    # Feed audio
    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    return output_data


# ==================================================================================================


def greedy_decode(ctc_labes: np.ndarray) -> str:
    """Simple greedy decoding of the network's prediction"""

    # Greedy ctc decoding
    values = np.argmax(ctc_labes, axis=-1)
    merged = [values[0]]
    for v in values[1:]:
        if v != merged[-1]:
            merged.append(v)
    merged = [v for v in merged if not v == len(alphabet)]
    gd_text = "".join([idx2char[v] for v in merged])

    # Text might contain spaces at the beginning or end
    gd_text = gd_text.strip()

    return gd_text


# ==================================================================================================


def test_tflite(checkpoint_file):

    print("\n\nLoading model ...")
    interpreter = tflite.Interpreter(
        model_path=checkpoint_file, num_threads=mp.cpu_count()
    )
    print("Input details:", interpreter.get_input_details())

    print("\nTranscribing the audio ...")
    # Print label if one of the test wavs is used
    if wav_path.startswith("/Scribosermo/exporting/data/test_"):
        lang = wav_path[-6:-4]
        if lang in labels:
            print("Label:            ", labels[lang])

    # Now run the transcription
    audio = load_audio(wav_path)
    prediction = predict(interpreter, audio)
    text = greedy_decode(prediction[0])
    print("Prediction greedy: {}".format(text))


# ==================================================================================================


def test_pb(checkpoint_dir):

    print("\nLoading model ...")
    model = tf.keras.models.load_model(checkpoint_dir)
    model.summary()
    print("Metadata: {}".format(model.get_metadata()))

    print("\nTranscribing the audio ...")
    # Print label if one of the test wavs is used
    if wav_path.startswith("/Scribosermo/exporting/data/test_"):
        lang = wav_path[-6:-4]
        if lang in labels:
            print("Label:            ", labels[lang])

    # Now run the transcription
    audio = load_audio(wav_path)
    prediction = model.predict(audio, steps=1)
    text = greedy_decode(prediction[0])
    print("Prediction greedy: {}".format(text))


# ==================================================================================================


def main():

    if tf_enabled:
        test_pb(checkpoint_pb)

    if tflite_enabled:
        test_tflite(checkpoint_tflite_float32)
        test_tflite(checkpoint_tflite_float16)
        test_tflite(checkpoint_tflite_int8)


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("\nFINISHED")
