import multiprocessing as mp

import numpy as np
import soundfile as sf
import tensorflow as tf
import tflite_runtime.interpreter as tflite

# ==================================================================================================

export_dir = "/checkpoints/en/conformerctc-large/exported/"
checkpoint_pb = export_dir + "pb/"
checkpoint_tflite_float32 = export_dir + "model_float32.tflite"
checkpoint_tflite_float16 = export_dir + "model_float16.tflite"
checkpoint_tflite_int8 = export_dir + "model_int8.tflite"
wav_path = "/Scribosermo/exporting/data/test_en.wav"


# ==================================================================================================


def load_audio(path):
    """Load wav file with the required format"""

    audio, _ = sf.read(path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict(interpreter, audio):
    """Feed an audio signal with shape [1, len_signal] into the network and get the predictions"""

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    # Feed audio
    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    return output_data


# ==================================================================================================


def test_tflite(checkpoint_file, audio):

    print("\nLoading model ...")
    interpreter = tflite.Interpreter(
        model_path=checkpoint_file, num_threads=mp.cpu_count()
    )
    print("Input details:", interpreter.get_input_details())

    output = predict(interpreter, audio)
    print("\n" + checkpoint_file)
    print(output)


# ==================================================================================================


def test_pb(checkpoint_dir, audio):

    print("\nLoading model ...")
    model = tf.keras.models.load_model(checkpoint_dir)
    model.summary()
    print("Metadata: {}\n".format(model.get_metadata()))

    output = model.predict(audio)
    print("\n" + checkpoint_dir)
    print(output)


# ==================================================================================================


def main():

    audio = load_audio(wav_path)
    test_pb(checkpoint_pb, audio)
    test_tflite(checkpoint_tflite_float32, audio)
    test_tflite(checkpoint_tflite_float16, audio)
    test_tflite(checkpoint_tflite_int8, audio)


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("\nFINISHED")
