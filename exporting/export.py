import argparse
import json
import os

import pandas as pd
import tensorflow as tf
import tqdm
from tensorflow.python.framework.ops import (  # pylint: disable=no-name-in-module
    enable_eager_execution,
)

import model as exmodel
from scode import training, utils

# ==================================================================================================

metadata = {
    "network": "ConformerCTC-Large",
    "num_features": 80,
    "audio_sample_rate": 16000,
    "audio_window_samples": int(16000 * 0.025),
    "audio_step_samples": int(16000 * 0.01),
    "alphabet": [],
    "language": "",
}

datasetpath = "/data_prepared/en/librispeech/dev-clean_azce.csv"

# ==================================================================================================


def representative_dataset():
    """Used for improved quantization
    See: https://www.tensorflow.org/lite/performance/post_training_quantization"""

    def load_audio(sample):
        audio_binary = tf.io.read_file(sample["filepath"])
        audio, _ = tf.audio.decode_wav(audio_binary)
        audio = tf.squeeze(audio, axis=-1)
        return audio

    df = pd.read_csv(datasetpath, encoding="utf-8", sep="\t", keep_default_na=False)
    df = df[["filepath"]]
    df = df.sample(frac=1)

    ds = tf.data.Dataset.from_tensor_slices(dict(df))
    ds = ds.map(map_func=load_audio)
    ds = ds.batch(1)

    num_samples = 200
    for data in tqdm.tqdm(
        ds.take(num_samples), total=num_samples, desc="Dataset optimization"
    ):
        yield [tf.dtypes.cast(data, tf.float32)]


# ==================================================================================================


def export_tflite(model, save_path, optimize: bool, fullint: bool):

    converter = tf.lite.TFLiteConverter.from_keras_model(model)

    if optimize:
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
    if fullint:
        converter.representative_dataset = representative_dataset

    tflite_model = converter.convert()

    with open(save_path, "wb+") as file:
        file.write(tflite_model)


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Export model")
    parser.add_argument("--checkpoint_dir", type=str, required=True)
    parser.add_argument("--export_dir", type=str, required=True)
    parser.add_argument("--mode", type=str, required=True)
    args = parser.parse_args()

    # Eager execution is required for pb and tflite exports
    enable_eager_execution()

    if not os.path.isdir(args.export_dir):
        os.makedirs(args.export_dir)

    # Load exported model
    nn_model = training.load_exported_model(args.checkpoint_dir)

    # Load alphabet and language
    path = os.path.join(args.checkpoint_dir, "config_export.json")
    exported_config = utils.load_json_file(path)
    alphabet = utils.load_alphabet(exported_config)
    metadata["alphabet"] = json.dumps(alphabet)
    metadata["language"] = exported_config["language"]

    # Export alphabet
    alphabetpath = os.path.join(args.export_dir, "alphabet.json")
    with open(alphabetpath, "w+", encoding="utf-8") as file:
        json.dump(alphabet, file, indent=2)

    # from scode import nets
    # net_config = {
    #     "name": "conformerctc",
    #     "nlayers": 16,
    #     "dimension": 176,
    #     "attention_heads": 4,
    # }
    # nn_model = nets.conformerctc.MyModel(
    #     c_input=metadata["num_features"], c_output=129, netconfig=net_config
    # )
    # nn_model.build(input_shape=(1, 458, metadata["num_features"]))
    # nn_model.summary()
    # _ = nn_model.predict(tf.random.normal([1, 458, 80]), steps=1)
    # # tf.saved_model.save(nn_model, args.export_dir + "pb/")
    # # nn_model = None

    if args.mode == "pb":
        # Export as .pb model
        model_pb = exmodel.MyModel(nn_model, metadata)
        model_pb.build(input_shape=(None, None))
        model_pb.summary()

        tf.keras.models.save_model(
            model_pb, args.export_dir + "pb/", include_optimizer=False
        )

    elif args.mode == "tflite":
        # Export as .tflite model
        model_tl = exmodel.MyModel(nn_model, metadata, int8_zero_guards=False)
        model_tl.build(input_shape=(None, None))
        model_tl.summary()

        export_tflite(
            model_tl,
            args.export_dir + "model_float32.tflite",
            optimize=False,
            fullint=False,
        )
        export_tflite(
            model_tl,
            args.export_dir + "model_float16.tflite",
            optimize=True,
            fullint=False,
        )

        # Rebuild again with different zero guards
        model_tl = exmodel.MyModel(nn_model, metadata, int8_zero_guards=True)
        model_tl.build(input_shape=(None, None))
        model_tl.summary()

        export_tflite(
            model_tl,
            args.export_dir + "model_int8.tflite",
            optimize=True,
            fullint=False,
        )


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("FINISHED")
