import multiprocessing as mp
import random
import time

import numpy as np
import soundfile as sf
import tqdm

# When testing on different devices not all runtimes might be installed
tf_enabled = False
try:
    import tensorflow as tf
    import tensorflow.lite as tflite

    tf_enabled = True
except ImportError:
    pass

if not tf_enabled:
    import tflite_runtime.interpreter as tflite


# ==================================================================================================

export_dir = "/checkpoints/en/conformerctc-large/exported/"
checkpoint_pb = export_dir + "pb/"
checkpoint_tflite_float32 = export_dir + "model_float32.tflite"
checkpoint_tflite_float16 = export_dir + "model_float16.tflite"
checkpoint_tflite_int8 = export_dir + "model_int8.tflite"
wav_path = "/Scribosermo/exporting/data/test_en.wav"

# ==================================================================================================


def load_audio(path):
    """Load wav file with the required format"""

    audio, _ = sf.read(path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def predict(interpreter, audio):
    """Feed an audio signal with shape [1, len_signal] into the network and get the predictions"""

    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # Enable dynamic shape inputs
    interpreter.resize_tensor_input(input_details[0]["index"], audio.shape)
    interpreter.allocate_tensors()

    # Feed audio
    interpreter.set_tensor(input_details[0]["index"], audio)
    interpreter.invoke()

    output_data = interpreter.get_tensor(output_details[0]["index"])
    return output_data


# ==================================================================================================


def init_random(predict_func):

    print("\nRunning some initialization steps ...")
    for _ in tqdm.tqdm(range(5)):
        length = random.randint(1234, 123456)
        data = np.random.uniform(-1, 1, [1, length]).astype(np.float32)
        _ = predict_func(data)


# ==================================================================================================


def timed_random(predict_func):

    print("\nPredicting random signal samples ...")

    signal_lengths = [
        1234,
        12345,
        123456,
        1234567,
        23456,
        345678,
        45678,
        567890,
        67890,
        78901,
        89012,
        90123,
    ]
    signals = [
        np.random.uniform(-1, 1, [1, length]).astype(np.float32)
        for length in signal_lengths
    ]

    time_start = time.time()
    for signal in tqdm.tqdm(signals):
        _ = predict_func(signal)
    time_stop = time.time()

    len_audio = sum(signal_lengths) / 16000
    dur_model = time_stop - time_start
    msg = "Length of random signal was {:.3f}s, the predictions took {:.3f}s"
    print(msg.format(len_audio, dur_model))
    msg = "The Real-Time-Factor is {:.3f}"
    print(msg.format(dur_model / len_audio))


# ==================================================================================================


def timed_audio(predict_func):

    print("\nPredicting real audio sample ...")

    time_start = time.time()
    audio = load_audio(wav_path)
    time_audio = time.time()
    _ = predict_func(audio)
    time_model = time.time()

    dur_audio = time_audio - time_start
    dur_model = time_model - time_audio
    len_audio = float(sf.info(wav_path).duration)
    msg = "Length of audio was {:.3f}s, loading it took {:.3f}s, the prediction took {:.3f}s"
    print(msg.format(len_audio, dur_audio, dur_model))
    msg = "The Real-Time-Factor is {:.3f}"
    print(msg.format(dur_model / len_audio))


# ==================================================================================================


def test_tflite(checkpoint_file):

    print("\n\nLoading model ...")
    time_start = time.time()
    interpreter = tflite.Interpreter(
        model_path=checkpoint_file, num_threads=mp.cpu_count()
    )
    time_stop = time.time()
    print("Loading the model took {:.3f}s".format(time_stop - time_start))
    print("Input details:", interpreter.get_input_details())

    # Running the tests
    predict_func = lambda x: predict(interpreter, x)
    init_random(predict_func)
    timed_random(predict_func)
    timed_audio(predict_func)


# ==================================================================================================


def test_pb(checkpoint_dir):

    print("\nLoading model ...")
    time_start = time.time()
    model = tf.keras.models.load_model(checkpoint_dir)
    model.summary()
    time_stop = time.time()
    print("Loading the model took {:.3f}s".format(time_stop - time_start))
    print("Metadata: {}".format(model.get_metadata()))

    # Running the tests
    predict_func = lambda x: model.predict(x, steps=1)
    init_random(predict_func)
    timed_random(predict_func)
    timed_audio(predict_func)


# ==================================================================================================


def main():

    if tf_enabled:
        test_pb(checkpoint_pb)

    test_tflite(checkpoint_tflite_float32)
    test_tflite(checkpoint_tflite_float16)
    test_tflite(checkpoint_tflite_int8)


# ==================================================================================================

if __name__ == "__main__":
    main()
    print("\nFINISHED")
