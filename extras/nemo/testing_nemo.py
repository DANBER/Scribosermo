import json

import fastwer
import nemo.collections.asr as nemo_asr
import numpy as np
import onnxruntime
import soundfile as sf
import torch
import tqdm
from nemo.collections.asr.data.audio_to_text import AudioToCharDataset
from nemo.collections.asr.metrics.wer import WER

# ==================================================================================================

test_manifest = "/dsp_nemo/data/test.json"

# Make sure you convert the transcriptions to lower case after exporting them with corcua
# test_manifest = "/data_prepared/en/librispeech/nm_dev-clean.json"


# ==================================================================================================


def to_numpy(tensor):
    if tensor.requires_grad:
        nt = tensor.detach().cpu().numpy()
    else:
        nt = tensor.cpu().numpy()
    return nt


# ==================================================================================================


def print_tensor(name: str, tensor):
    print("Tensor:", name)
    arr = to_numpy(tensor)
    print(" Shape:", arr.shape)
    print(" Minimum:", np.min(arr, axis=-1))
    print(" Maximum:", np.max(arr, axis=-1))
    print(" Mean:", np.mean(arr, axis=-1))
    print(" Values:", arr)


# ==================================================================================================


def setup_transcribe_dataloader(cfg, vocabulary):
    config = {
        "manifest_filepath": cfg["manifestpath"],
        "sample_rate": 16000,
        "labels": vocabulary,
        "batch_size": cfg["batch_size"],
        "trim_silence": False,
        "shuffle": False,
    }
    dataset = AudioToCharDataset(
        manifest_filepath=config["manifest_filepath"],
        labels=config["labels"],
        sample_rate=config["sample_rate"],
        int_values=config.get("int_values", False),
        augmentor=None,
        max_duration=config.get("max_duration", None),
        min_duration=config.get("min_duration", None),
        max_utts=config.get("max_utts", 0),
        blank_index=config.get("blank_index", -1),
        unk_index=config.get("unk_index", -1),
        normalize=config.get("normalize_transcripts", False),
        trim=config.get("trim_silence", True),
        parser=config.get("parser", "en"),
    )
    return torch.utils.data.DataLoader(
        dataset=dataset,
        batch_size=config["batch_size"],
        collate_fn=dataset.collate_fn,
        drop_last=config.get("drop_last", False),
        shuffle=False,
        num_workers=config.get("num_workers", 0),
        pin_memory=config.get("pin_memory", False),
    )


# ==================================================================================================


def print_input(nemo_model_path):

    model = nemo_asr.models.EncDecCTCModel.restore_from(restore_path=nemo_model_path)
    config = {"batch_size": 1, "manifestpath": test_manifest}
    temporary_datalayer = setup_transcribe_dataloader(config, model.decoder.vocabulary)

    np.set_printoptions(edgeitems=10)
    print("Vocabulary:", model.decoder.vocabulary)

    for test_batch in temporary_datalayer:
        print(test_batch)
        print(to_numpy(test_batch[0][0]))

        processed_signal, processed_signal_len = model.preprocessor(
            input_signal=test_batch[0].to(model.device),
            length=test_batch[1].to(model.device),
        )

        nps = to_numpy(processed_signal)
        nps = np.transpose(nps, [0, 2, 1])
        print(nps)
        print(nps.shape, processed_signal_len)
        break


# ==================================================================================================


def transcribe_nemo(nemo_model_path):

    # Get filenames from manifest
    filepaths = []
    with open(test_manifest, "r") as f:
        for line in f:
            item = json.loads(line)
            filepaths.append(item["audio_filepath"])

    model = nemo_asr.models.EncDecCTCModel.restore_from(restore_path=nemo_model_path)

    transcriptions = model.transcribe(filepaths, batch_size=1)
    print("Transcribed:", transcriptions, "\n")

    # # Edit /NeMo/nemo/collections/asr/models/ctc_models.py that it returns logits as well
    # # Also drop del statement some lines above and comment out the function's return type
    # _, logits = model.transcribe(filepaths, batch_size=1)
    # logits = to_numpy(logits)
    # return logits


# ==================================================================================================


def run_onnx_test(nemo_model_name, batch_size: int, print_predictions: bool):

    config = {"batch_size": batch_size, "manifestpath": test_manifest}
    model = nemo_asr.models.EncDecCTCModel.restore_from(
        restore_path="/dsp_nemo/models/" + nemo_model_name + ".nemo"
    )
    ort_session = onnxruntime.InferenceSession(
        "/dsp_nemo/models/" + nemo_model_name + ".onnx",
        providers=["CPUExecutionProvider"],
    )
    temporary_datalayer = setup_transcribe_dataloader(config, model.decoder.vocabulary)
    wer_calc = WER(
        vocabulary=model.decoder.vocabulary,
        batch_dim_index=0,
        use_cer=False,
        ctc_decode=True,
    )

    np.set_printoptions(edgeitems=10)
    for test_batch in tqdm.tqdm(temporary_datalayer):

        processed_signal, processed_signal_len = model.preprocessor(
            input_signal=test_batch[0].to(model.device),
            length=test_batch[1].to(model.device),
        )
        ort_inputs = {ort_session.get_inputs()[0].name: to_numpy(processed_signal)}

        if "conformer" in nemo_model_name:
            ort_inputs[ort_session.get_inputs()[1].name] = to_numpy(
                processed_signal_len
            )

        if print_predictions:
            print("Inputs:", ort_inputs)
            print([v.shape for k, v in ort_inputs.items()])

        ologits = ort_session.run(None, ort_inputs)
        logits = np.asarray(ologits)[0]
        greedy_predictions = torch.from_numpy(logits).argmax(dim=-1, keepdim=False)

        decoded = wer_calc.ctc_decoder_predictions_tensor(greedy_predictions)
        decoded = decoded[0].replace("▁", " ")

        label = wer_calc.ctc_decoder_predictions_tensor(test_batch[2])
        label = label[0].replace("▁", " ")

        if print_predictions:
            print("Logits:", np.asarray(logits))
            print(logits.shape)
            print("Greedy:", greedy_predictions)
            print("Decoded:", decoded)
            print("Label:", label)

    return logits


# ==================================================================================================


def load_audio(wav_path):
    """Load wav file with the required format"""

    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


def run_full_test_onnx(nemo_model_name, jsonlpath: str):

    with open(jsonlpath, "r", encoding="utf-8") as file:
        samples = file.readlines()

    model = nemo_asr.models.EncDecCTCModel.restore_from(
        restore_path="/dsp_nemo/models/" + nemo_model_name + ".nemo"
    )
    wer_calc = WER(
        vocabulary=model.decoder.vocabulary,
        batch_dim_index=0,
        use_cer=False,
        ctc_decode=True,
    )
    ort_session = onnxruntime.InferenceSession(
        "/dsp_nemo/models/" + nemo_model_name + ".onnx",
        # providers=["CPUExecutionProvider"],
    )

    gd_wers = []
    gd_cers = []
    for i, sample in enumerate(tqdm.tqdm(samples)):
        sample = json.loads(sample)

        audio = load_audio(sample["audio_filepath"])
        audio_length = np.array([audio.shape[1]])

        # A bit different to output one below
        processed_signal, processed_signal_len = model.preprocessor(
            input_signal=torch.from_numpy(audio).to(model.device),
            length=torch.from_numpy(audio_length).to(model.device),
        )
        ort_inputs = {ort_session.get_inputs()[0].name: to_numpy(processed_signal)}

        if "conformer" in nemo_model_name:
            ort_inputs[ort_session.get_inputs()[1].name] = to_numpy(
                processed_signal_len
            )

        ologits = ort_session.run(None, ort_inputs)
        logits = np.asarray(ologits)[0]
        greedy_predictions = torch.from_numpy(logits).argmax(dim=-1, keepdim=False)

        decoded = wer_calc.ctc_decoder_predictions_tensor(greedy_predictions)
        decoded = decoded[0].replace("▁", " ").strip()
        label = sample["text"].strip().lower()

        gd_wer = fastwer.score_sent(decoded, label)
        gd_cer = fastwer.score_sent(decoded, label, char_level=True)
        gd_wers.append(gd_wer)
        gd_cers.append(gd_cer)

        if i % 100 == 0:
            print("")
            print("=Decoded=:", decoded)
            print("=Label===:", label)
            print("WER:", gd_wer, "- CER:", gd_cer)

    wer = np.array(gd_wers).mean()
    cer = np.array(gd_cers).mean()
    print("Accuracy: {} WER - {} CER".format(wer, cer))


# ==================================================================================================


def run_full_test_nemo(nemo_model_name, jsonlpath: str):

    with open(jsonlpath, "r", encoding="utf-8") as file:
        samples = file.readlines()

    model = nemo_asr.models.EncDecCTCModel.restore_from(
        restore_path="/dsp_nemo/models/" + nemo_model_name + ".nemo"
    )

    gd_wers = []
    gd_cers = []
    for i, sample in enumerate(tqdm.tqdm(samples)):
        sample = json.loads(sample)

        transcriptions = model.transcribe([sample["audio_filepath"]], batch_size=1)
        decoded = transcriptions[0].strip().lower()
        label = sample["text"].strip().lower()

        gd_wer = fastwer.score_sent(decoded, label)
        gd_cer = fastwer.score_sent(decoded, label, char_level=True)
        gd_wers.append(gd_wer)
        gd_cers.append(gd_cer)

        if i % 100 == 0:
            print("")
            print("=Decoded=:", decoded)
            print("=Label===:", label)
            print("WER:", gd_wer, "- CER:", gd_cer)

    wer = np.array(gd_wers).mean()
    cer = np.array(gd_cers).mean()
    print("Accuracy: {} WER - {} CER".format(wer, cer))


# ==================================================================================================


def compare_outputs(nemo_model_name):

    out_nm = transcribe_nemo("/dsp_nemo/models/" + nemo_model_name + ".nemo")
    out_ox = run_onnx_test(nemo_model_name, 1, True)

    msg = "Are Close {}: {}/{}"
    countclose = lambda a, b, t: np.sum(np.isclose(a, b, rtol=t, atol=t))
    print(msg.format("(1e-6)", countclose(out_nm, out_ox, 0.000001), out_ox.size))
    print(msg.format("(1e-5)", countclose(out_nm, out_ox, 0.00001), out_ox.size))
    print(msg.format("(1e-4)", countclose(out_nm, out_ox, 0.0001), out_ox.size))
    print(msg.format("(1e-3)", countclose(out_nm, out_ox, 0.001), out_ox.size))
    print(msg.format("(1e-2)", countclose(out_nm, out_ox, 0.01), out_ox.size))
    print(msg.format("(1e-1)", countclose(out_nm, out_ox, 0.1), out_ox.size))


# ==================================================================================================


def compare_pipelines(nemo_model_name):

    config = {"batch_size": 1, "manifestpath": test_manifest}
    model = nemo_asr.models.EncDecCTCModel.restore_from(
        restore_path="/dsp_nemo/models/" + nemo_model_name + ".nemo"
    )
    temporary_datalayer = setup_transcribe_dataloader(config, model.decoder.vocabulary)

    for test_batch in tqdm.tqdm(temporary_datalayer):
        processed_signal, processed_signal_len = model.preprocessor(
            input_signal=test_batch[0].to(model.device),
            length=test_batch[1].to(model.device),
        )
        inp_ox = to_numpy(processed_signal)[0]

    # Install package beforehand: `pip3 install -e /Scribosermo/training/`
    from scode import pipeline

    test_csv = "/dsp_nemo/data/test.csv"
    pl_config = {
        "alphabet_path": "/Scribosermo/data/en/alphabet.json",
        "audio_features": {
            "audio_sample_rate": 16000,
            "num_features": 80,
            "window_len": 0.025,
            "window_step": 0.01,
        },
        "training": {
            "sort_datasets": False,
        },
        "augmentations": {
            "signal": {
                "normalize_volume": {"use_train": False, "use_test": False},
                "dither": {"use_train": True, "use_test": False, "factor": 1e-05},
                "preemphasis": {
                    "use_train": True,
                    "use_test": True,
                    "coefficient": 0.97,
                },
            },
            "spectrogram": {},
            "features": {"normalize_features": {"use_train": True, "use_test": True}},
            # "features": {"normalize_features": {"use_train": False, "use_test": False}},
        },
    }

    tds = pipeline.create_pipeline(test_csv, 1, pl_config, mode="test")
    for samples in tds:
        inp_tf = samples["features"][0]
        # inp_tf = samples["signal"][0]
        # inp_tf = samples["spectrogram"][0]
        inp_tf = np.transpose(inp_tf)
        break

    print("OX:", inp_ox)
    print("TF:", inp_tf)

    msg = "Are Close {}: {}/{}"
    countclose = lambda a, b, t: np.sum(np.isclose(a, b, rtol=t, atol=t))
    print(msg.format("(1e-6)", countclose(inp_ox, inp_tf, 0.000001), inp_ox.size))
    print(msg.format("(1e-5)", countclose(inp_ox, inp_tf, 0.00001), inp_ox.size))
    print(msg.format("(1e-4)", countclose(inp_ox, inp_tf, 0.0001), inp_ox.size))
    print(msg.format("(1e-3)", countclose(inp_ox, inp_tf, 0.001), inp_ox.size))
    print(msg.format("(1e-2)", countclose(inp_ox, inp_tf, 0.01), inp_ox.size))
    print(msg.format("(1e-1)", countclose(inp_ox, inp_tf, 0.1), inp_ox.size))

    return inp_ox, inp_tf


# ==================================================================================================


def compare_pipeline_results(nemo_model_name):

    inp_ox, inp_tf = compare_pipelines(nemo_model_name)
    inp_ox = np.expand_dims(inp_ox, axis=0)
    inp_tf = np.expand_dims(inp_tf, axis=0)

    ort_session = onnxruntime.InferenceSession(
        "/dsp_nemo/models/" + nemo_model_name + ".onnx",
        providers=["CPUExecutionProvider"],
    )

    ort_inputs = {ort_session.get_inputs()[0].name: inp_ox}
    if "conformer" in nemo_model_name:
        ort_inputs[ort_session.get_inputs()[1].name] = np.array([inp_ox.shape[1]])
    ologits = ort_session.run(None, ort_inputs)
    out_ox = np.asarray(ologits)[0]

    ort_inputs = {ort_session.get_inputs()[0].name: inp_tf}
    if "conformer" in nemo_model_name:
        ort_inputs[ort_session.get_inputs()[1].name] = np.array([inp_tf.shape[1]])
    ologits = ort_session.run(None, ort_inputs)
    out_tf = np.asarray(ologits)[0]

    print("OX:", out_ox)
    print("TF:", out_tf)

    msg = "Are Close {}: {}/{}"
    countclose = lambda a, b, t: np.sum(np.isclose(a, b, rtol=t, atol=t))
    print(msg.format("(1e-6)", countclose(out_ox, out_tf, 0.000001), out_ox.size))
    print(msg.format("(1e-5)", countclose(out_ox, out_tf, 0.00001), out_ox.size))
    print(msg.format("(1e-4)", countclose(out_ox, out_tf, 0.0001), out_ox.size))
    print(msg.format("(1e-3)", countclose(out_ox, out_tf, 0.001), out_ox.size))
    print(msg.format("(1e-2)", countclose(out_ox, out_tf, 0.01), out_ox.size))
    print(msg.format("(1e-1)", countclose(out_ox, out_tf, 0.1), out_ox.size))


# ==================================================================================================

# print_input("/dsp_nemo/models/QuartzNet5x5LS-En.nemo")
# print_input("/dsp_nemo/models/stt_en_conformer_ctc_small_old.nemo")
# print_input("/dsp_nemo/models/stt_es_conformer_ctc_large.nemo")
# print_input("/dsp_nemo/models/stt_fr_conformer_ctc_large.nemo")
# print_input("/dsp_nemo/models/stt_fr_no_hyphen_conformer_ctc_large.nemo")
# print_input("/dsp_nemo/models/stt_de_conformer_ctc_large.nemo")
transcribe_nemo("/dsp_nemo/models/stt_en_conformer_ctc_small_old.nemo")
# run_onnx_test("stt_en_conformer_ctc_small_old", batch_size=1, print_predictions=True)
# run_full_test_onnx("stt_en_conformer_ctc_small_old", "data_prepared/en/librispeech/nm_test-clean.json")
# run_full_test_nemo("stt_en_conformer_ctc_small_old", "data_prepared/en/librispeech/nm_test-clean.json")
# run_onnx_test("QuartzNet5x5LS-En", batch_size=1, print_predictions=True)
# run_onnx_test("QuartzNet5x5LS-En", batch_size=16, print_predictions=False)
# compare_outputs("stt_en_conformer_ctc_small")
# compare_outputs("stt_en_conformer_ctc_small_old")
# compare_pipelines("stt_en_conformer_ctc_small")
# compare_pipelines("stt_en_conformer_ctc_small_old")
# compare_pipeline_results("stt_en_conformer_ctc_small")
# compare_pipeline_results("stt_en_conformer_ctc_small_old")
