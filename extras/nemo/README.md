# NeMo Model Conversion

The official ASR tutorial (containing also instructions for transfer-learning) can be found under this
[link](https://colab.research.google.com/github/NVIDIA/NeMo/blob/master/tutorials/asr/01_ASR_with_NeMo.ipynb). \
The goal here is to use the pretrained NeMo models from Nvidia with our tensorflow implementation.

- Get model [here](https://ngc.nvidia.com/catalog/models/nvidia:nemospeechmodels/files) and save it in `models` folder.

- Optionally finetune the model to a different vocabulary:

  ```bash
  # Build a new vocabulary tokenizer
  python3 /NeMo/scripts/tokenizers/process_asr_text_tokenizer.py \
    --manifest="/data_prepared/en/librispeech/nm_train-all.json" \
    --data_root="/dsp_nemo/models/tokenizers/en-char/" \
    --tokenizer="spe" --spe_type="char" --log

  # Finetune the model
  python3 /dsp_nemo/finetune_nemo.py

  # Alternatively, from outside of the container
  nohup ./Scribosermo/extras/nemo/finetune_nemo.sh > ./Scribosermo/extras/nemo/nohup.out 2>&1 &
  ```

- Convert `.nemo` to `.onnx`:

  ```bash
  docker build -f ./Scribosermo/extras/nemo/Containerfile_Nemo -t dsp_nemo ./Scribosermo/

  docker run --gpus all -it --rm -p 8888:8888 -p 6007:6006 \
    --ulimit memlock=-1 --ulimit stack=67108864  --shm-size=8g \
    --volume `pwd`/Scribosermo/extras/nemo/:/dsp_nemo/ \
    --volume `pwd`/Scribosermo/data/:/Scribosermo/data/ \
    --volume `pwd`/Scribosermo/training/:/Scribosermo/training/ \
    --volume `pwd`/data_prepared/:/data_prepared/ \
    --device=/dev/snd dsp_nemo

  # Convert pretrained model. Optset 12 was required for tensorflow onnx backend.
  # Tested with NeMo git commit: 633674898132b315b4f2eaf3761de391d3d0c1f4
  python3 /NeMo/scripts/export.py \
    /dsp_nemo/models/stt_en_conformer_ctc_small.nemo /dsp_nemo/models/stt_en_conformer_ctc_small.onnx \
    --runtime-check --onnx-opset 12

  # Test model and some debugging for our pipeline
  python3 /dsp_nemo/testing_nemo.py

  # Adjust these files for pipeline/model debugging
  # Copy the two tensor debugging functions from script above into this file
  # For comparisons, comment out dither augmentation in the preprocessor
  nano /NeMo/nemo/collections/asr/parts/preprocessing/features.py  # FilterbankFeatures->forward
  nano /NeMo/nemo/collections/asr/models/ctc_models.py  # EncDecCTCModel->transcribe

  # File for onnx verification debugging
  /opt/conda/lib/python3.8/site-packages/nemo/core/classes/exportable.py
  ```

- Go to https://netron.app/ and look at the graph structure.
  This is also a nice way to compare two graph implementations with each other.

- Build and start conversion container:

  ```bash
  docker build -f ./Scribosermo/extras/nemo/Containerfile_Onnx -t onnx-tf ./Scribosermo/

  docker run --rm --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 --gpus all \
    --volume `pwd`/Scribosermo/:/Scribosermo/ \
    --volume `pwd`/Scribosermo/extras/nemo/:/dsp_nemo/ \
    --volume `pwd`/checkpoints/:/checkpoints/ -it onnx-tf

  # Speed up exporting by disabling the gpu
  export CUDA_VISIBLE_DEVICES=""
  ```

- Check out that the tf-data pipeline and nemo's input preprocessor both return the same audio features:

  ```bash
  python3 /dsp_nemo/testing_nemo.py
  python3 /dsp_nemo/testing_input.py
  ```

- Test that the onnx model is working and has the same output with both tensorflow and pytorch backends:

  ```bash
  python3 /dsp_nemo/testing_onnxtf.py
  ```

- Transfer the pretrained weights:

  ```bash
  # Uncomment the required calls at the bottom
  python3 /dsp_nemo/transfer_weights.py
  ```

- Go to the exported checkpoint, edit the `config_export.json` and then do a full test run.

- Convert between `.pb` and `.onnx`:

  ```bash
  # From .pb to .onnx (can be used for better visualisation in above web-tool)
  python3 -m tf2onnx.convert --opset 12 --saved-model /checkpoints/tmp/ --output /dsp_nemo/models/tfmodel.onnx

  # From .onnx to .pb (loading it into our tensorflow model didn't work)
  onnx-tf convert -i /dsp_nemo/models/QuartzNet5x5LS-En.onnx -o /dsp_nemo/models/tfpb/
  ```
