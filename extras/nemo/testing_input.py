import librosa
import numpy as np
import soundfile as sf
import tensorflow as tf

from scode import pipeline

# ==================================================================================================

test_csv = "/Scribosermo/extras/nemo/data/test.csv"

pl_config = {
    "alphabet_path": "/Scribosermo/data/en/alphabet.json",
    "audio_features": {
        "audio_sample_rate": 16000,
        "num_features": 80,
        "window_len": 0.025,
        "window_step": 0.01,
    },
    "training": {
        "sort_datasets": False,
    },
    "augmentations": {
        "signal": {
            "normalize_volume": {"use_train": False, "use_test": False},
            "dither": {"use_train": True, "use_test": False, "factor": 1e-05},
            "preemphasis": {"use_train": True, "use_test": True, "coefficient": 0.97},
        },
        "spectrogram": {},
        "features": {"normalize_features": {"use_train": True, "use_test": True}},
    },
}


# ==================================================================================================


def test_same():
    np.random.seed(666)
    np.set_printoptions(precision=5, suppress=True)

    audio_length_seconds = 2.123413
    sample_rate = 16000
    audio_frames_length = int(sample_rate * audio_length_seconds)

    frame_length = 400
    frame_step = 160
    n_fft = 512
    signal = np.random.random((1, audio_frames_length))

    x = tf.reshape(signal, [-1])
    x = tf.cast(x, dtype=tf.float32)
    pad_amount = 2 * (frame_length - frame_step)
    pad_amount = frame_length
    x = tf.pad(x, [[pad_amount // 2, pad_amount // 2]], "REFLECT")
    x = tf.reshape(x, [1, -1])

    f = tf.signal.frame(x, frame_length, frame_step, pad_end=False)
    w = tf.signal.hann_window(frame_length, periodic=False)

    # print(f)
    # print(w)
    stfts = tf.signal.rfft(f * w, fft_length=[n_fft])

    tf_results = tf.abs(stfts)

    lr_results = librosa.core.stft(
        y=signal.reshape((-1)),
        n_fft=n_fft,
        hop_length=frame_step,
        win_length=frame_length,
        window=w.numpy(),
        center=True,
    )

    lr_results = np.abs(lr_results)

    difference = np.abs(tf_results - lr_results.T)
    print(
        "Differences:\nmin:",
        np.min(difference),
        "max:",
        np.max(difference),
        "mean:",
        np.mean(difference),
        "std:",
        np.std(difference),
    )


# ==================================================================================================


def load_audio(wav_path):
    """Load wav file with the required format"""

    audio, _ = sf.read(wav_path, dtype="int16")
    audio = audio / (np.iinfo(np.int16).max + 1)
    audio = np.expand_dims(audio, axis=0)
    audio = audio.astype(np.float32)
    return audio


# ==================================================================================================


def normalize_volume(signal):
    """Normalize volume to range [-1,1]"""

    gain = tf.math.divide_no_nan(1.0, tf.reduce_max(tf.abs(signal)))
    signal = signal * gain
    return signal


# ==================================================================================================


def preemphasis(signal, coef=0.97):
    """Emphasizes high-frequency signal components"""

    psig = signal[1:] - coef * signal[:-1]
    signal = tf.concat([[signal[0]], psig], axis=0)
    return signal


# ==================================================================================================


def per_feature_norm(features):
    """Normalize features per channel/frequency"""
    f_mean = tf.math.reduce_mean(features, axis=0)

    # Calculate our own std, because pytorch uses unbiasing, which is approximated by reducing the
    # sample number by one
    f_std = features - tf.expand_dims(f_mean, axis=0)
    f_std = tf.reduce_sum(f_std**2, axis=0)
    n = tf.cast(tf.shape(features)[0], dtype=f_std.dtype) - 1.0
    f_std = tf.sqrt(f_std / n)

    print(f_mean)
    print(f_std + 1e-5)

    features = (features - f_mean) / (f_std + 1e-5)
    return features


# ==================================================================================================


def lrs_features(audio_path):
    """Taken from:
    https://github.com/NVIDIA/OpenSeq2Seq/blob/master/open_seq2seq/data/speech2text/speech_utils.py"""

    # _, signal = wave.read(audio_path)
    # signal = signal.astype(np.float32)
    # audio_binary = tf.io.read_file(audio_path)
    # audio1, _ = tf.audio.decode_wav(audio_binary)
    audio2 = load_audio(audio_path)

    # print(audio1)
    # print(audio2)

    signal = tf.reshape(audio2, [-1]).numpy()
    print("raw_audio", signal)

    # signal = normalize_volume(signal)
    signal = preemphasis(signal)

    print("signal", signal)

    frame_length = 400
    frame_step = 160
    n_fft = 512

    w = tf.signal.hann_window(frame_length, periodic=False)

    signal = signal.numpy()
    stfts = librosa.core.stft(
        signal,
        n_fft=n_fft,
        hop_length=frame_step,
        win_length=frame_length,
        window=w.numpy(),
        center=True,
    )
    # print("stft", stfts.T)
    # print(stfts.T.shape)

    spectrogram = np.abs(stfts) ** 2

    print("spec", spectrogram.T)
    print(spectrogram.T.shape)

    # lmw_matrix = tf.signal.linear_to_mel_weight_matrix(
    #     num_mel_bins=80,
    #     num_spectrogram_bins=257,
    #     sample_rate=16000,
    #     lower_edge_hertz=0,
    #     upper_edge_hertz=16000 / 2,
    # )
    # lmw_matrix = tf.transpose(lmw_matrix)
    # print("melb1", lmw_matrix)

    # Build and apply Mel filter
    # mel_basis = librosa.filters.mel(16000, 512, n_mels=80, fmin=0, fmax=int(16000 / 2),  htk=True, norm=None)
    mel_basis = librosa.filters.mel(16000, 512, n_mels=80, fmin=0, fmax=int(16000 / 2))

    mel = np.dot(mel_basis, spectrogram)
    features = np.log(mel + 2**-24).T

    # print("mel", mel.T)
    # print("logmel", features)
    # print(features.shape)

    features = per_feature_norm(features).numpy()
    return features


# ==================================================================================================


def debug_input(csv_path):
    tds = pipeline.create_pipeline(csv_path, 1, pl_config, mode="test")
    np.set_printoptions(edgeitems=10)

    for samples in tds:
        print(samples)

        # afp = samples["filepath"].numpy()[0]
        # lfeat = lrs_features(afp)
        # print("feat", lfeat)
        # print(lfeat.shape)

        break


# ==================================================================================================

# test_same()
debug_input(test_csv)
