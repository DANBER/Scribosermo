import numpy as np
import onnx
from onnx_tf.backend import prepare

from scode import pipeline

# ==================================================================================================

test_csv = "/Scribosermo/extras/nemo/data/test.csv"

pl_config = {
    "alphabet_path": "/Scribosermo/data/en/alphabet.json",
    "audio_features": {
        "audio_sample_rate": 16000,
        "num_features": 80,
        "window_len": 0.025,
        "window_step": 0.01,
    },
    "training": {
        "sort_datasets": False,
    },
    "augmentations": {
        "signal": {
            "normalize_volume": {"use_train": False, "use_test": False},
            "dither": {"use_train": True, "use_test": False, "factor": 1e-05},
            "preemphasis": {"use_train": True, "use_test": True, "coefficient": 0.97},
        },
        "spectrogram": {},
        "features": {"normalize_features": {"use_train": True, "use_test": True}},
    },
}

# ==================================================================================================


def print_onnx_infos(onnx_path: str):
    onnx_model = onnx.load(onnx_path)
    print("Input:", onnx_model.graph.input)
    print("Output:", onnx_model.graph.output)


# ==================================================================================================


def test_random_input(onnx_path: str):
    onnx_model = onnx.load(onnx_path)
    onnxtf_model = prepare(onnx_model)

    inp = np.random.uniform(low=-1, high=1, size=[1, 80, 458]).astype(np.float32)
    ort_inputs = {"audio_signal": inp, "length": np.array([458])}
    out = onnxtf_model.run(ort_inputs)
    print("Result: ", out)
    print("Shape: ", out.logprobs.shape)


# ==================================================================================================


def test_pipeline_input(onnx_path: str):
    onnx_model = onnx.load(onnx_path)
    onnxtf_model = prepare(onnx_model)

    tds = pipeline.create_pipeline(test_csv, 1, pl_config, mode="test")
    np.set_printoptions(edgeitems=10)

    for samples in tds:
        ort_inputs = {
            "audio_signal": np.transpose(samples["features"].numpy(), [0, 2, 1]),
            "length": np.array([samples["features"].numpy().shape[1]]),
        }

        print("Inputs:", ort_inputs)
        print([v.shape for _, v in ort_inputs.items()])
        out = onnxtf_model.run(ort_inputs)
        print("Result: ", out)
        print("Shape: ", out.logprobs.shape)
        print("Greedy:", np.argmax(out, axis=-1))


# ==================================================================================================

if __name__ == "__main__":
    # print_onnx_infos("/dsp_nemo/models/stt_en_conformer_ctc_small.onnx")
    # test_random_input("/dsp_nemo/models/stt_en_conformer_ctc_small.onnx")
    test_pipeline_input("/dsp_nemo/models/stt_en_conformer_ctc_large.onnx")
