#! /bin/bash

docker run --gpus all --rm -p 8888:8888 -p 6006:6006 \
  --ulimit memlock=-1 --ulimit stack=67108864  --shm-size=8g \
  --volume "$(pwd)"/Scribosermo/extras/nemo/:/dsp_nemo/ \
  --volume "$(pwd)"/Scribosermo/data/:/Scribosermo/data/ \
  --volume "$(pwd)"/data_prepared/:/data_prepared/ \
  --device=/dev/snd dsp_nemo \
  python3 /dsp_nemo/finetune_nemo.py
