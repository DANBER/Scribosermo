import copy

# import glob
import json
import os

# import nemo
import nemo.collections.asr as nemo_asr
import pytorch_lightning as ptl
import torch

# import wget
# from nemo.collections.asr.metrics.wer import word_error_rate
from nemo.utils import exp_manager
from omegaconf import OmegaConf, open_dict

# import subprocess
# import tarfile


# ==================================================================================================

alphabet_path = "/Scribosermo/data/en/alphabet.json"
with open(alphabet_path, "r", encoding="utf-8") as file:
    alphabet = json.load(file)

train_manifest = "/data_prepared/en/librispeech/nm_train-all.json"
eval_manifest = "/data_prepared/en/librispeech/nm_dev-clean.json"
test_manifest = "/data_prepared/en/librispeech/nm_dev-clean.json"

# nemo_model_path = "/dsp_nemo/models/stt_en_conformer_ctc_medium.nemo"
nemo_model_path = "/dsp_nemo/models/finetune/confomerctc-medium/finetuned.nemo"
tokenizer_path = "/dsp_nemo/models/tokenizers/en-char/tokenizer_spe_char_v1024/"
freeze_encoder = False
change_vocab = False
save_dir = "/dsp_nemo/models/finetune/confomerctc-medium/"
save_path = save_dir + "finetuned2.nemo"

if torch.cuda.is_available():
    gpus = 2
else:
    gpus = 0

EPOCHS = 10

# ==================================================================================================


def main():

    model = nemo_asr.models.EncDecCTCModel.restore_from(restore_path=nemo_model_path)

    # Change vocabulary
    if change_vocab:
        model.change_vocabulary(
            new_tokenizer_dir=tokenizer_path, new_tokenizer_type="bpe"
        )

    # Optionally freeze encoder
    if freeze_encoder:
        model.encoder.freeze()

    # Setup train, validation, test configs
    cfg = copy.deepcopy(model.cfg)
    # Setup train, validation, test configs
    with open_dict(cfg):
        # Train dataset
        cfg.train_ds.manifest_filepath = train_manifest
        cfg.train_ds.batch_size = 8
        cfg.train_ds.num_workers = 8
        cfg.train_ds.pin_memory = True
        cfg.train_ds.use_start_end_token = True
        cfg.train_ds.trim_silence = True
        cfg.train_ds.is_tarred = False

        # Validation dataset
        cfg.validation_ds.manifest_filepath = eval_manifest
        cfg.validation_ds.batch_size = 8
        cfg.validation_ds.num_workers = 8
        cfg.validation_ds.pin_memory = True
        cfg.validation_ds.use_start_end_token = True
        cfg.validation_ds.trim_silence = True
        cfg.validation_ds.is_tarred = False

        # Test dataset
        cfg.test_ds.manifest_filepath = test_manifest
        cfg.test_ds.batch_size = 8
        cfg.test_ds.num_workers = 8
        cfg.test_ds.pin_memory = True
        cfg.test_ds.use_start_end_token = True
        cfg.test_ds.trim_silence = True
        cfg.test_ds.is_tarred = False

    # Setup new tokenizer
    cfg.tokenizer.dir = tokenizer_path
    cfg.tokenizer.type = "bpe"
    model.cfg.tokenizer = cfg.tokenizer

    with open_dict(model.cfg.optim):
        model.cfg.optim.lr = 0.025
        model.cfg.optim.weight_decay = 0.001
        model.cfg.optim.sched.warmup_steps = (
            None  # Remove default number of steps of warmup
        )
        model.cfg.optim.sched.warmup_ratio = 0.10  # 10 % warmup
        model.cfg.optim.sched.min_lr = 1e-9

    model.spec_augmentation = model.from_config_dict(model.cfg.spec_augment)

    # model._wer.use_cer = True
    model._wer.log_prediction = True

    # Trainer
    trainer = ptl.Trainer(
        gpus=gpus,
        max_epochs=EPOCHS,
        accumulate_grad_batches=1,
        checkpoint_callback=False,
        logger=False,
        log_every_n_steps=25,
        check_val_every_n_epoch=3,
        strategy="ddp",
    )

    # Setup model with the trainer
    model.set_trainer(trainer)

    # Setup data loaders with new configs, has to be after the trainer
    model.setup_training_data(cfg.train_ds)
    model.setup_multiple_validation_data(cfg.validation_ds)
    model.setup_multiple_test_data(cfg.test_ds)

    # Finally, update the model's internal config
    model.cfg = model._cfg

    config = exp_manager.ExpManagerConfig(
        exp_dir=save_dir,
        name="ASR-Char-Model-Language-En",
        checkpoint_callback_params=exp_manager.CallbackParams(
            monitor="val_wer",
            mode="min",
            always_save_nemo=True,
            save_best_model=True,
        ),
    )
    config = OmegaConf.structured(config)

    # Now train it
    trainer.fit(model)

    model.save_to(f"{save_path}")
    print(f"Model saved at path : {os.getcwd() + os.path.sep + save_path}")


# ==================================================================================================


if __name__ == "__main__":
    main()
