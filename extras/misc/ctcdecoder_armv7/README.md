# Crossbuild `ds_ctcdecoder` for `armv7l`

Build DeepSpeech's building container:

```bash
cd DeepSpeech/
make Dockerfile.build
cd ..

# Now edit the Dockerfile and remove the KenLM building part at the bottom
# With the KenLM installation I had some problems building the native-client again

docker build -t dsbuild - < DeepSpeech/Dockerfile.build
```

Enable container cross-building:

```bash
sudo podman run --security-opt label=disable --rm --privileged multiarch/qemu-user-static --reset -p yes
```

Build `swig` for raspbian:

```bash
podman build --cgroup-manager=cgroupfs -f Scribosermo/extras/misc/ctcdecoder_armv7/Containerfile_DecoderCrossbuild1 -t xbuildctc1

podman run --rm -it \
  --volume "$(pwd)"/Scribosermo/extras/misc/ctcdecoder_armv7/:/Scribosermo/extras/misc/ctcdecoder_armv7/ \
  xbuildctc1

cp /ds-swig.tar.gz /Scribosermo/extras/misc/ctcdecoder_armv7/
```

Now build the `ds_ctcdecoder.whl` package and extract it from the container:

```bash
docker build -f Scribosermo/extras/misc/ctcdecoder_armv7/Containerfile_DecoderCrossbuild2 -t xbuildctc2 Scribosermo/extras/misc/ctcdecoder_armv7/

docker run --rm -it \
  --volume "$(pwd)"/Scribosermo/extras/misc/ctcdecoder_armv7/:/Scribosermo/extras/misc/ctcdecoder_armv7/ \
  xbuildctc2

cp /DeepSpeech/native_client/ctcdecode/dist/*.whl /Scribosermo/extras/misc/ctcdecoder_armv7/
```
