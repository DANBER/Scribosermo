#!/bin/sh
#SBATCH --partition=small-cpu  # One of "small-cpu", "big-cpu", "gpu"
#SBATCH --job-name SSP  # Job Name
#SBATCH --cpus-per-task 8
#SBATCH --ntasks 1
#SBATCH --mem 24000
#SBATCH --time=1000:00:00  # Time after which the job will be aborted
#
#
# Actual singularity call, mounted folder and call to script
singularity exec \
  --bind ~/Jaco-Assistant/checkpoints/:/checkpoints/ \
  --bind /cfs/share/cache/db_xds/data_original/:/data_original/ \
  --bind /cfs/share/cache/db_xds/data_prepared/:/data_prepared/ \
  --bind ~/Jaco-Assistant/Scribosermo/:/Scribosermo/ \
  --bind ~/Jaco-Assistant/corcua/:/corcua/ \
  ~/Jaco-Assistant/images/scribosermo.sif \
  /bin/bash -c 'python3 /Scribosermo/preprocessing/noise_to_csv.py'
