# Scribosermo

_Train fast Speech-to-Text networks in different languages._ An overview of the approach was presented in the paper [Scribosermo: Fast Speech-to-Text models for German and other Languages](https://arxiv.org/pdf/2110.07982.pdf).

<div align="center">
    <img src="media/quartznet_architecture.png" alt="quartznet graph" width="35%"/>
    <img src="media/deepspeech1_architecture.png" alt="deepspeech1 graph" width="25%"/>
    <img src="media/conformer_architecture.png" alt="quartznet graph" width="35%"/>
</div>

<br/>

[![pipeline status](https://gitlab.com/Jaco-Assistant/Scribosermo/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/Scribosermo/-/commits/master)
[![coverage report](https://gitlab.com/Jaco-Assistant/Scribosermo/badges/master/coverage.svg)](https://gitlab.com/Jaco-Assistant/Scribosermo/-/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/Scribosermo/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/Scribosermo/-/commits/master)

<br/>

## Usage

Note: This repository is focused on training STT-networks, but you can find some inference examples in the [examples](examples/) directory.

Requirements are:

- Computer with a modern gpu and working nvidia+docker setup
- Basic knowledge in python and deep-learning
- A lot of training data in your required language \
  (preferable >100h for fine-tuning and >1000h for new languages)

#### General infos

File structure will look as follows:

```text
my_speech2text_folder
    checkpoints
    corcua                 <- Library for datasets
    data_original
    data_prepared
    Scribosermo            <- This repository
```

Clone [corcua](https://gitlab.com/Jaco-Assistant/corcua):

```bash
git clone https://gitlab.com/Jaco-Assistant/corcua.git
```

Build and run docker container:

```bash
docker build -f Scribosermo/Containerfile -t scribosermo ./Scribosermo/

./Scribosermo/run_container.sh
```

<br/>

#### Download and prepare voice data

Follow [readme](preprocessing/README.md) in `preprocessing` directory for preparing the voice data.

#### Create the language model

Follow [readme](langmodel/README.md) in `langmodel` directory for generating the language model.

#### Training

Follow [readme](training/README.md) in `training` directory for training your network. \
Note: Currently training is only supported with character-based, but not with sentencpiece-style text labels.

#### Exporting

For easier inference follow the [readme](exporting/README.md) in `exporting` directory.

<br/>

## Datasets and Networks

You can find more details about the currently used datasets [here](preprocessing/README.md#Datasets).

|          |     |     |     |     |
| -------- | --- | --- | --- | --- |
| Language | DE  | EN  | ES  | FR  |
| Datasets | 37  | 3   | 8   | 8   |

<br>

Implemented networks:
[DeepSpeech1](https://arxiv.org/pdf/1412.5567.pdf),
[QuartzNet](https://arxiv.org/pdf/1910.10261.pdf),
[Conformer](https://arxiv.org/pdf/2005.08100.pdf)(CTC-version, sentencepiece+characterbased),
[ContextNet](https://arxiv.org/pdf/2005.03191.pdf)(simplified),
[DeepSpeech2](https://arxiv.org/pdf/1512.02595.pdf),
[Jasper](https://arxiv.org/pdf/1904.03288.pdf)

Notes on the networks:

- Not every network is fully tested, but each could be trained with one single audio file.
- Some networks might differ slightly from their paper implementations.

Supported networks with their trainable parameter count (using English alphabet), and the multiply-add floting point operations (length and feature dependent, measured with the example audio file and conformer's input features):

|         |             |                                                                                                                                                                                   |                                                                                                                                                                                                                                      |                  |             |                                |
| ------- | ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------- | ----------- | ------------------------------ |
| Network | DeepSpeech1 | QuartzNet&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | ConformerCTC&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | ContextNetSimple | DeepSpeech2 | Jasper&nbsp;&nbsp;&nbsp;&nbsp; |
| Config  |             | 5x5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / 15x5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / +LSTM                                                                                                    | 16x176x4 / 18x256x4 / 18x512x8                                                                                                                                                                                                       | 0.8              |             | 10x5-DR                        |
| Params  | 51.6M       | 6.7M&nbsp;&nbsp; / 18.9M&nbsp;&nbsp; / 21.5M                                                                                                                                      | 13.0M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / 30.0M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / 121M                                                                                                                                   | 21.4M            | 120M        | 333M                           |
| Flops   | 8.28G       | 1.53G&nbsp; / 4.32G&nbsp;&nbsp; / 4.32G                                                                                                                                           | 2.33G&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / 5.21G&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; / 20.2G                                                                                                                            | 3.92G            | 1.97G       | 76.2G                          |

<br>

## Pretrained Checkpoints and Language Models

By default, the checkpoints are provided under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) licence,
but a lot of datasets have extra conditions (for example non-commercial use only) which might also have to be applied.
The QuartzNet and Conformer models might be double licenced by [Nvidia](https://github.com/NVIDIA/NeMo/discussions/1681), because they use their pretrained weights.
Please check this yourself for the models you want to use.

You can find the full training results of below models and from other experiments in a different file: [Training Results](media/RESULTS.md).
All models below were tested on the _CommonVoice_ dataset (but different release versions), except the English Quartznet model which was tested on _LibriSpeech_.

If you want to inspect or continue the training of an older model, switch the repository branch to the corresponding architecture tag.
The current code version might not be compatible anymore.

The models use different alphabets depending on the architecture. You can find them next to the uploaded checkpoints.

### Conformer:

#### charbased

_Note: Even though the character-based models have a generally lower transcription accuracy than the sentence-piece models, they achieve better results in usecases with very uncommon words, see [SmartSpeaker-Benchmark](https://gitlab.com/Jaco-Assistant/Benchmark-Jaco/-/merge_requests/19)._

- **German**:
  - ConformerCTC-Large (Greedy-WER: 11.76%, Lm-WER: 7.79%): [Link](https://zenodo.org/record/7352170)
- **English**:
  - ConformerCTC-Small (Greedy-WER: 20.52%, Lm-WER: 17.76%): [Link](https://zenodo.org/record/7352237)
  - ConformerCTC-Large (Greedy-WER: 17.68%, Lm-WER: 14.38%): [Link](https://zenodo.org/record/7352213)
- **Spanish**:
  - ConformerCTC-Large (Greedy-WER: 14.82%, Lm-WER: 8.73%): [Link](https://zenodo.org/record/7352194)
- **French**:
  - ConformerCTC-Large (Greedy-WER: 14.38%, Lm-WER: 10.21%): [Link](https://zenodo.org/record/7352202)

#### sentencepiece

- **German**:
  - ConformerCTC-Large (Greedy-WER: 7.32%, Lm-WER: 4.04%): [Link](https://zenodo.org/record/6821495)
  - Langmodel: [Ken1M](https://zenodo.org/record/7074145),
    [TEVR](https://huggingface.co/fxtentacle/wav2vec2-xls-r-1b-tevr/tree/main/language_model)
- **English**:
  - ConformerCTC-Small (Greedy-WER: 18.16%, Lm-WER: 16.22%): [Link](https://zenodo.org/record/6818517)
  - ConformerCTC-Large (Greedy-WER: 9.43%, Lm-WER: 9.06%): [Link](https://zenodo.org/record/6821199)
  - Langmodel: [Ken1M](https://zenodo.org/record/6976286)
- **Spanish**:
  - ConformerCTC-Large (Greedy-WER: 7.46%, Lm-WER: 5.68%): [Link](https://zenodo.org/record/6822093)
  - Langmodel: [Ken1M](https://zenodo.org/record/7074159)
- **French**:
  - ConformerCTC-Large (Greedy-WER: 10.19%, Lm-WER: 8.13%): [Link](https://zenodo.org/record/6821314)
  - Langmodel: [Ken1M](https://zenodo.org/record/6976301)

### QuartzNet:

- **German**:
  - Quartznet15x5, CV only (WER: 7.5%, Train: ~720h): [Link](https://www.mediafire.com/folder/rrse5ydtgdpvs/cv-wer0077)
  - Quartznet15x5, D37CV (WER: 6.6%, Train: ~2370h): [Link](https://www.mediafire.com/folder/jh5unptizgzou/d37cv-wer0066)
  - Scorer: [TCV](https://www.mediafire.com/file/xb2dq2roh8ckawf/kenlm_de_tcv.scorer/file),
    [D37CV](https://www.mediafire.com/file/pzj8prgv2h0c8ue/kenlm_de_all.scorer/file),
    [PocoLg](https://www.mediafire.com/file/b64k0uqv69ehe9p/de_pocolm_large.scorer/file)
- **English**:
  - Quartznet5x5 (WER: 4.5%): [Link](https://www.mediafire.com/folder/3c0a353nlppkv/qnet5)
  - Quartznet15x5 (WER: 3.7%): [Link](https://www.mediafire.com/folder/eb340s2ab4sv0/qnet15)
  - Scorer: [Link](https://github.com/mozilla/DeepSpeech/releases/tag/v0.9.3) (to DeepSpeech)
- **Spanish**:
  - Quartznet15x5, CV only (WER: 10.5%, Train: ~266h): [Link](https://www.mediafire.com/folder/1peahr4b17t8i/cv-wer0105)
  - Quartznet15x5, D8CV (WER: 10.0%), Train: ~817h: [Link](https://www.mediafire.com/folder/2x2kdq3wlbg0h/d8cv-wer0100)
  - Scorer: [KenSm](https://www.mediafire.com/file/h38hmax7wnkxqfd/kenlm_es_n12.scorer/file),
    [PocoLg](https://www.mediafire.com/file/pwt95u2wik8gr5s/es_pocolm_d8cv.scorer/file)
- **French**:
  - Quartznet15x5, CV only (WER: 12.1%, Train: ~558h): [Link](https://www.mediafire.com/folder/bee6yoirkcoui/cv-wer0121)
  - Quartznet15x5, D7CV (WER: 11.0%, Train: ~1028h): [Link](https://www.mediafire.com/folder/hesl65v0369b4/d7cv-wer0110)
  - Scorer: [KenSm](https://www.mediafire.com/file/pcj322gp5ddpfhd/kenlm_fr_n12.scorer/file),
    [PocoLg](https://www.mediafire.com/file/55qv3bpu6z0m1p9/fr_pocolm_d7cv.scorer/file)
- **Italian**:
  - Quartznet15x5, D5CV (WER: 11.5%, Train: ~360h): [Link](https://www.mediafire.com/folder/atxlkc8xxzosq/d5cv-wer0115)
  - Scorer: [PocoLg](https://www.mediafire.com/file/cuf9adxqqxbqlbu/it_pocolm_d5cv.scorer/file)

### Mozilla's DeepSpeech:

- **German**:
  - D17S5 training and some older checkpoints (WER: 0.128, Train: ~1582h, Test: ~41h):
    [Link](https://drive.google.com/drive/folders/1oO-N-VH_0P89fcRKWEUlVDm-_z18Kbkb?usp=sharing)
- **Spanish**:
  - CCLMTV training (WER: 0.165, Train: ~660h, Test: ~25h):
    [Link](https://drive.google.com/drive/folders/1-3UgQBtzEf8QcH2qc8TJHkUqCBp5BBmO?usp=sharing)
- **French**:
  - CCLMTV training (WER: 0.195, Train: ~787h, Test: ~25h):
    [Link](https://drive.google.com/drive/folders/1Nk_1uFVwM7lj2RQf4PaQOgdAdqhiKWyV?usp=sharing)
- **Italian**:
  - CLMV training (WER: 0.248 Train: ~257h, Test: ~21h):
    [Link](https://drive.google.com/drive/folders/1BudQv6nUvRSas69SpD9zHN-TmjGyedaK?usp=sharing)
- **Polish**:
  - CLM training (WER: 0.034, Train: ~157h, Test: ~6h):
    [Link](https://drive.google.com/drive/folders/1_hia1rRmmsLRrFIHANH4254KKZhY3p1c?usp=sharing)

<br/>

## Citation

Please cite Scribosermo if you found it helpful for your research or business.

```bibtex
@article{
  scribosermo,
  title={Scribosermo: Fast Speech-to-Text models for German and other Languages},
  author={Bermuth, Daniel and Poeppel, Alexander and Reif, Wolfgang},
  journal={arXiv preprint arXiv:2110.07982},
  year={2021}
}
```

<br/>

## Contribution

You can contribute to this project in multiple ways:

- Help to solve the open issues
- Implement new networks or augmentation options
- Train new models or improve the existing \
  (Requires a gpu and a lot of time, or multiple gpus and some time)
- Experiment with the language models
- Add a new language:

  - Extend `data/` directory with the `alphabet` and `langdicts` files
  - Add speech datasets
  - Find text corpora for the language model

#### Tests

See [readme](tests/README.md) in `tests` directory for testing instructions.
