# Building the language model

Instructions to collect sample sentences and build an additional language model for improved accuracy.

<br/>

Download sentence collection datasets from [tuda](http://ltdata1.informatik.uni-hamburg.de/kaldi_tuda_de/),
[europarl+news](https://www.statmt.org/wmt13/translation-task.html) or [librispeech](http://www.openslr.org/11):

```bash
export LANGUAGE="de"
mkdir -p /data_original/texts/${LANGUAGE}/
mkdir -p /data_prepared/texts/${LANGUAGE}/

# German
cd /data_original/texts/${LANGUAGE}/
wget "http://ltdata1.informatik.uni-hamburg.de/kaldi_tuda_de/German_sentences_8mil_filtered_maryfied.txt.gz" -O tuda_sentences.txt.gz
gzip -d tuda_sentences.txt.gz

# German or French or Spanish
cd /data_original/texts/
wget "https://www.statmt.org/wmt13/training-monolingual-nc-v8.tgz" -O news-commentary.tgz
tar zxvf news-commentary.tgz && mv training/news-commentary-v8.${LANGUAGE} ${LANGUAGE}/news-commentary-v8.txt
rm news-commentary.tgz && rm -r training/
wget "https://www.statmt.org/wmt13/training-monolingual-europarl-v7.tgz" -O europarl.tgz
tar zxvf europarl.tgz && mv training/europarl-v7.${LANGUAGE} ${LANGUAGE}/europarl-v7.txt
rm europarl.tgz && rm -r training/
# If you have enough space you can also download the other years (2007-2011)
wget "https://www.statmt.org/wmt13/training-monolingual-news-2012.tgz" -O news-2012.tgz
tar zxvf news-2012.tgz && mv training-monolingual/news.2012.${LANGUAGE}.shuffled ${LANGUAGE}/news.2012.txt
rm news-2012.tgz && rm -r training-monolingual/

# English
wget https://www.openslr.org/resources/11/librispeech-lm-norm.txt.gz -O librispeech_sentences.txt.gz
gzip -d librispeech_sentences.txt.gz
```

In addition to above sentence collections, we can extract more sentences from our own datasets: \
(Only use the training partitions for that)

```bash
# Run in container
export LANGUAGE="de"

python3 /Scribosermo/langmodel/extract_transcripts.py \
  --input_csv /data_prepared/${LANGUAGE}/librispeech/train-all.csv \
  --output_txt /data_original/texts/${LANGUAGE}/librispeech.txt
```

<br/>

Prepare the sentences.

```bash
# Run in container
export LANGUAGE="de"

python3 /Scribosermo/langmodel/prepare_vocab.py \
  --input_dir /data_original/texts/${LANGUAGE}/ \
  --output_dir /data_prepared/texts/${LANGUAGE}/

# Combine to single file and shuffle it
mkdir -p /data_prepared/langmodel/${LANGUAGE}/tmp/
echo /data_prepared/texts/${LANGUAGE}/*.txt | xargs cat | shuf > /data_prepared/langmodel/${LANGUAGE}/all.txt
```

<br/>

Create an `arpa`-format language model with KenLM:

```bash
# Run in container
export LANGUAGE="de"
cd /data_prepared/langmodel/${LANGUAGE}/

# Build arpa file
/kenlm/build/bin/lmplz --order 5 --temp_prefix /tmp/ --memory 95% --prune 0 0 1 \
  --text all.txt --arpa tmp/lm.arpa

# Collect and save top-k words
python3 /Scribosermo/langmodel/collect_topk.py --top_k 1000000 \
  --input_file all.txt --output_file tmp/vocab.txt

# Filter the language model with our vocabulary
/kenlm/build/bin/filter single \
  model:tmp/lm.arpa tmp/kenlm.arpa < tmp/vocab.txt
```

<br/>

Optimize the model by converting it into quantized binary format:

```bash
# Reduce model size
/kenlm/build/bin/build_binary -a 255 -q 8 -v trie \
  /data_prepared/langmodel/${LANGUAGE}/tmp/kenlm.arpa \
  /data_prepared/langmodel/${LANGUAGE}/tmp/kenlm.bin
```

Clean up intermediate files:

```bash
cd /data_prepared/langmodel/${LANGUAGE}/
mv tmp/kenlm.bin kenlm.bin
mv tmp/vocab.txt vocab.txt

rm -rf /data_prepared/langmodel/${LANGUAGE}/tmp/
```
